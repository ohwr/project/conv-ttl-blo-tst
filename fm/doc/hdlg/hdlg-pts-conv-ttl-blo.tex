%==============================================================================
% Document header
%==============================================================================
\documentclass[a4paper,11pt]{article}
\usepackage[
  colorlinks = true,
  linkcolor  = black,
  citecolor  = black,
  urlcolor   = blue,
]{hyperref}

\usepackage{graphicx}
\usepackage{multirow}
\usepackage{longtable}

\usepackage{color}

\usepackage[toc,page]{appendix}

% Color package
\usepackage[usenames,dvipsnames,table]{xcolor}

% Header and footer customization
\usepackage{fancyhdr}
\setlength{\headheight}{15.2pt}
\pagestyle{fancy}
\fancyhead[L]{\nouppercase{\leftmark}}
\fancyhead[R]{} 
\renewcommand{\footrulewidth}{0.4pt}

%==============================================================================
% Start of document
%==============================================================================
\begin{document}

%------------------------------------------------------------------------------
% Title
%------------------------------------------------------------------------------
\include{cern-title}

%------------------------------------------------------------------------------
% Licensing info
%------------------------------------------------------------------------------
\pagebreak

\thispagestyle{empty}

\addcontentsline{toc}{section}{Licensing information}
\section*{Licensing information}

\noindent
This document is licensed under a Creative Commons Attribution-ShareAlike 4.0
International License. If you have not received a copy of the license along with this
work, see \\
\url{http://creativecommons.org/licenses/by-sa/4.0/}

%------------------------------------------------------------------------------
% Revision history
%------------------------------------------------------------------------------
\section*{Revision history}
\addcontentsline{toc}{section}{Revision history}

\centerline
{
  \rowcolors{2}{white}{gray!25}
  \begin{tabular}{l c p{.6\textwidth}}
  \hline
  \multicolumn{1}{c}{\textbf{Date}} & \multicolumn{1}{c}{\textbf{Version}} & \multicolumn{1}{c}{\textbf{Change}} \\
  \hline
  03-05-2013 & 0.1 & First draft \\
  28-06-2013 & 0.2 & Changed title page, layout, and updated info \\
  07-08-2013 & 0.3 & Added test logic section, additional docs \\
  11-12-2014 & 1.0 & Updated document according to converter board documentation template, added licensing
                     information, updated information according to changes in HDL, added memory map
                     as appendix, removed redundant information \\
  26-10-2017 & 1.1 & Added Test hwvertest and updated wishbone regsiter maps\\
  \hline
  \end{tabular}
}

%------------------------------------------------------------------------------
% Table of contents, list of figs, tables
%------------------------------------------------------------------------------
\pagebreak
\pdfbookmark[1]{\contentsname}{toc}
\tableofcontents

\pagebreak
\listoffigures
\listoftables

%------------------------------------------------------------------------------
% List of abbreviations
%------------------------------------------------------------------------------
\pagebreak
\section*{List of abbreviations}
\begin{tabular}{l l}
  FPGA   & Field-Programmable Gate Array \\
  I$^2$C & Inter-Integrated Circuit \\
  PTS    & Production Test Suite \\
  SFP    & Small Form-factor Pluggable (transceiver) \\
  VME    & VERSAmodule Eurocard \\
\end{tabular}
\addcontentsline{toc}{section}{List of abbreviations}

%==============================================================================
% SEC: Intro
%==============================================================================
\pagebreak
\section{Introduction}
\label{sec:intro}
Production Test Suite (PTS) is the environment designed for running functionality tests
on boards at the manufacturing site, right after production. It assures that boards comply
with a minimum set of quality rules in terms of soldering, mounting and PCB fabrication
process.

This document presents a high-level view of the firmware implemented on the FPGA for the 
PTS project for the CONV-TTL-BLO board. More details can be found by consulting
the PTS gateware, which can be obtained by cloning the git repository for the PTS project~\cite{pts-repo}.

%------------------------------------------------------------------------------
% SUBSEC: Additional docs
%------------------------------------------------------------------------------
\subsection{Additional documentation}

\begin{itemize}
  \item CONV-TTL-BLO User Guide \cite{ctb-ug}
  \item CONV-TTL-BLO Hardware Guide \cite{ctb-hwguide}
  \item CONV-TTL-BLO HDL Guide \cite{ctb-hdlguide}
  \item CONV-TTL-BLO PTS User Guide \cite{pts-userguide}
  \item CONV-TTL-BLO PTS Hardware Guide \cite{pts-hwguide}
\end{itemize}

%======================================================================================
% SEC: PTS system
%======================================================================================
\section{PTS system}
\label{sec:pts-sys}

The PTS system (Figure~\ref{fig:pts-sys}) is contained within a rack containing an ELMA
crate, the laptop with the PTS software installed on it and all other accessories necessary
for running tests. More information about the PTS system can be found in the PTS user
guide~\cite{pts-userguide}.

\begin{figure}[h]
  \centerline{\includegraphics[width=.9\textwidth]{fig/pts-sys}}
  \caption{PTS System}
  \label{fig:pts-sys}
\end{figure}

The ELMA crate within the PTS contains a system monitor (SysMon) board that monitors
voltage levels, temperatures and controls fan speeds for the crate fans. The SysMon
can be accessed via Telnet to send commands to the VME boards inside the crate.

Using such commands, the CONV-TTL-BLO board can be accessed through the serial
(I$^2$C) interface on the VME P1 connector. The PTS software on the PTS laptop connects
to the crate through Telnet and sends \textit{readreg} and \textit{writereg}
commands to the crate; the commands are translated by the SysMon to I$^2$C transfers
to the CONV-TTL-BLO. More information about this can be found in \cite{ctb-ug} and
\cite{sysmon-i2c}.

%======================================================================================
% SEC: Gateware Overview
%======================================================================================
\section{Gateware overview}
\label{sec:overview}

A block diagram of the PTS gateware is shown in Figure~\ref{fig:hdl-bd}. The PTS software running
on a computer writes and reads registers implemented in the PTS gateware running on the CONV-TTL-BLO
on-board FPGA. As described in the following sections, writing and reading to specific registers
in the gateware result in the running of the steps necessary to test a CONV-TTL-BLO device under test.

For a complete reference of the registers accessible in the PTS gateware, see the complete memory map
for the gateware in Appendix~\ref{app:memmap}.

\begin{figure}[h]
  \centerline{\includegraphics[width=1.1\textwidth]{fig/hdl-bd}}
  \caption{\label{fig:hdl-bd} Gateware block diagram}
\end{figure}

%======================================================================================
% SEC: Test logic
%======================================================================================
\section{Test logic}
\label{sec:test-logic}

%--------------------------------------------------------------------------------------
% SUBSEC: Test 00
%--------------------------------------------------------------------------------------
\subsection{Test 00 -- PCB version test}

The PTS software verifies whether the version of the PCB is the one expected to 
be tested with this PTS. Each PTS is prepared for a particular version of PCB 
and it cannot be used to test other version of hardware. 

The test reads the PCB version provided in an HWVERS register (see Annex~\ref{app:pts-regs-csr}).
If the provided version is different than the expected version, no further tests is
run and PTS is stopped.

\textbf{This test is required to pass for the PTS to run the remaining tests.}

%--------------------------------------------------------------------------------------
% SUBSEC: Test 01
%--------------------------------------------------------------------------------------
\subsection{Test 01 -- DAC and clocks test}

The logic for this test is shown in Figure~\ref{fig:dac-test} and can be summarized as follows:

\begin{itemize}
  \item the PTS software controls (via OpenCores SPI master modules~\cite{spi}) the two DACs on the
  CONV-TTL-BLO
  \item the DACs control the tuning inputs of the on-board oscillators, which are input to
  the FPGA
  \item two counters are driven by these clocks; the values of these  counters (Appendix~\ref{app:clk-info})
  can be read by the PTS software
  \item the change in frequency of the oscillators can be observed on the rate of change
  of the two counters
  \item this rate of change is monitored by the PTS software
\end{itemize}

\begin{figure}[h]
  \centerline{\includegraphics[width=0.9\textwidth]{fig/dac-test}}
  \caption{\label{fig:dac-test} DAC and clocks test logic}
\end{figure}

%--------------------------------------------------------------------------------------
% SUBSEC: Test 02
%--------------------------------------------------------------------------------------  
\subsection{Test 02 -- Front panel LEDs test}

The PTS software sets the CHLEDT and STLEDT bits in the PTS control and status register
(CSR -- see Appendix~\ref{app:pts-regs-csr}), which starts a sequence counter inside the
gateware. Each pulse or status LED is turned on based on the value of this counter.

%--------------------------------------------------------------------------------------
% SUBSEC: Test 03
%--------------------------------------------------------------------------------------
\subsection{Test 03 -- TTL pulse and switches test}

The logic for this test is shown in Figure~\ref{fig:ttl-test} and can be summarized as follows:

\begin{itemize}
\item the PTS software sets the TTLPT bit in the PTS control and status register (CSR -- see Appendix~\ref{app:pts-regs-csr})
\item this enables a general-purpose pulse generator component on TTL CH1 to generate
1~$\mu$s pulses with a period of 500~ms
\item the pulses are replicated through the channel daisy-chain~\cite{pts-userguide} from
CH1 to INV-TTL CH D
\item CH2 through INV-TTL CH D (CH10 in VHDL) have pulse generator components~\cite{conv-common-gw-ohwr}
which generate 1~$\mu$s pulses when there is a pulse on the input
\item each channel has one input and output pulse counter; each counter gets incremented when
there is a rising edge on the input or output
\item the current values of these counters can be read by the PTS software from the pulse counter
registers (see Appendix~\ref{app:pulse-cnt})
\end{itemize}

For the switch test:
\begin{itemize}
  \item the user sets the switches as indicated in the CONV-TTL-BLO PTS User Guide~\cite{pts-userguide}
  \item the PTS software reads the CSR.SWITCH field (see Appendix~\ref{app:pts-regs-csr}) to
  confirm proper switch setting
\end{itemize}
            
\begin{figure}[h]
  \centerline{\includegraphics[width=1.1\textwidth]{fig/ttl-test}}
  \caption{\label{fig:ttl-test} TTL pulse test logic}
\end{figure}

%--------------------------------------------------------------------------------------
% SUBSEC: Test 04
%--------------------------------------------------------------------------------------
\pagebreak
\subsection{Test 04 -- Blocking pulse and RTM detection lines test}

The logic for this test is shown in Figure~\ref{fig:blo-test} and can be summarized as follows:

\begin{itemize}
\item the PTS software sets the CSR.REARPT bit (see Appendix~\ref{app:pts-regs-csr})
\item this enables six \textit{pulse\_gen\_gp} components to generate time-multiplexed 1~${\mu}s$ pulses;
each channel generates a pulse 100~ms after the previous, with 600~ms period:
  \begin{itemize}
  \item CH1 -- 0~ms
  \item CH2 -- 100~ms
  \item CH3 -- 200~ms
  \item CH4 -- 300~ms
  \item CH5 -- 400~ms
  \item CH6 -- 500~ms
  \end{itemize}
\item the PTS software clears the CSR.REARPT bit after some time
\item blocking pulses are generated in this manner and are fed back to
blocking inputs via the RTM Interface Tester \cite{pts-hwguide}
\item each channel has one input and output pulse counter; each counter gets incremented when
there is a rising edge on the input or output
\item the PTS software reads the pulse counter registers (Section~\ref{app:pulse-cnt}) to check
that the number of pulses that are sent at outputs are received back at inputs, according
to the connection diagram on the RTM Interface Tester
\end{itemize}

\begin{figure}[hbtp]
  \centerline{\includegraphics[width=.8\textwidth]{fig/blo-test}}
  \caption{\label{fig:blo-test} Blocking pulse test logic}
\end{figure}

For testing the RTM detection lines and rear-panel LED lines (Figure~\ref{fig:rtm-test}):
\begin{itemize}
  \item the PTS software writes the RLEDT bit in the PTS CSR
  \item this bit sets all rear LED lines
  \item the rear LED lines are looped back to the RTM detection lines through the RTM
  Interface Tester~\cite{pts-hwguide}
  \item the PTS software reads the RTM field in the PTS CSR to check for the correct value
\end{itemize}

\begin{figure}[h]
  \centerline{\includegraphics[width=.7\textwidth]{fig/rtm-test}}
  \caption{\label{fig:rtm-test} Rear-panel LED lines and RTM detection lines test logic}
\end{figure}

%--------------------------------------------------------------------------------------
% SUBSEC: Test 05
%--------------------------------------------------------------------------------------
\subsection{Test 05 -- Thermometer chip test}

The PTS software interacts with the OpenCores one-wire master core~\cite{onewire}
to access the DS18B20U+ temperature chip for retrieving the unique board ID and the
temperature at the time the test is run.

%--------------------------------------------------------------------------------------
% SUBSEC: Test 06
%--------------------------------------------------------------------------------------
\subsection{Test 06 -- SFP EEPROM test}

An OpenCores I$^2$C master component~\cite{i2c-master} communicates to the SFP connector and retrieves
its internal identifier. The PTS software drives this communication by interacting with the
I$^2$C master and checks the SFP identifier for correct value.

%--------------------------------------------------------------------------------------
% SUBSEC: Test 07
%--------------------------------------------------------------------------------------
\subsection{Test 07 -- SPF link test}

The PTS software communicates with MiniNIC and endpoint components~\cite{wr-cores} to
initialize them and check whether the link goes up through the SFP loopback connector.

%======================================================================================
% SEC: Folder structure
%======================================================================================
\section{Folder structure}
\label{sec:fold-struct}

The folder structure is shown in Table~\ref{tbl:fold-struct}.

\begin{table}[h]
  \rowcolors{2}{white}{gray!25}
  \caption{\label{tbl:fold-struct} Folder structure for the PTS gateware}
  \centerline{
  \begin{tabular}{l p{.7\textwidth}}
  \hline
  \multicolumn{1}{c}{\textbf{Folder}} & \multicolumn{1}{c}{\textbf{Description}} \\
  \hline
  ip\_cores/ & IP cores used as sub-modules within the git repository \\
  modules/ & PTS-specific modules instantiated in the top-level file \\
  syn/ & Contains the ISE project file and synthesis output files \\
  top/ & Contains the top-level VHDL file and the UCF definitions file for the project \\
  \hline
  \end{tabular}
  }
\end{table}

%======================================================================================
% SEC: Getting around the code
%======================================================================================
\section{Getting around the code}
\label{sec:get-around}

The most important part of the CONV-TTL-BLO PTS gateware is concentrated in the top-level file (\textit{pts.vhd}),
where submodules are instantiated and connected as needed by the tests. The code in the top-level file
is organized into code sections as shown in Figure~\ref{fig:hdl-arch}.

\begin{figure}[h]
  \centerline{\includegraphics[scale=1]{fig/hdl-arch}}
  \caption{Body of VHDL architecture}
  \label{fig:hdl-arch}
\end{figure}

%======================================================================================
% APP: Memory map
%======================================================================================
\pagebreak
\begin{appendices}

\section{Memory map}
\label{app:memmap}

The full memory map of the PTS gateware is shown in Table~\ref{tbl:memmap}. Details about each of
the modules in the table are given in the subsections below.

\begin{table}[h]
\caption{CONV-TTL-BLO PTS memory map}
\label{tbl:memmap}
\centerline
{
  \rowcolors{2}{white}{gray!25}
  \begin{tabular}{l l l p{.5\textwidth}}
  \hline
  \multicolumn{1}{c}{\textbf{Peripheral}} & \multicolumn{2}{c}{\textbf{Address range}} & \multicolumn{1}{c}{\textbf{Description}} \\
  \hline
  PTS registers  & 0x000 & 0x00f & PTS definition and control and status registers \\
  OneWire master & 0x010 & 0x01f & OpenCores OneWire master core~\cite{onewire} \\
  125-MHz DAC    & 0x020 & 0x07f & OpenCores SPI master core~\cite{spi} for the 125-MHz DAC\\
  20-MHz DAC     & 0x080 & 0x0ff & OpenCores SPI master core~\cite{spi} for the 20-MHz DAC\\
  125-MHz counter & 0x100 & 0x11f & Clock counters for the 125-MHz clock \\
  20-MHz counter & 0x120 & 0x13f & Clock counters for the 20-MHz clock \\
  I$^2$C master & 0x140 & 0x15f & OpenCores I$^2$C master core~\cite{i2c-master} for the SFP EEPROM \\
  Endpoint & 0x200 & 0x3ff & Endpoint~\cite{wr-cores} for communicating via the SFP \\
  MiniNIC & 0x400 & 0x7ff & MiniNIC~\cite{wr-cores} for communicating via the SFP \\
  DPRAM & 0x800 & 0xbff & Dual-port RAM for the endpoint and MiniNIC components \\
  Pulse counters & 0xc00 & 0xc7f & Pulse counters for pulse repetition tests \\
  \hline
  \end{tabular}
}
\end{table}

\include{pts_regs}

\include{clk_info}

\include{pulse_cnt_wb}

\end{appendices}

%======================================================================================
% Bibliography
%======================================================================================
\pagebreak
\bibliographystyle{ieeetr}
\bibliography{hdlg-pts-conv-ttl-blo}
\addcontentsline{toc}{section}{References}

\end{document}
