%==============================================================================
% Document header
%==============================================================================
\documentclass[a4paper,11pt]{article}

% Margin setup to use more of the page
\usepackage[top=3cm, bottom=3cm, left=2cm, right=2cm]{geometry}

% Color package
\usepackage[usenames,dvipsnames,table]{xcolor}

% Hyperrefs
\usepackage[
  colorlinks = true,
  linkcolor  = black,
  citecolor  = black,
  urlcolor   = blue,
]{hyperref}

% Longtable
\usepackage{longtable}

% Graphics, multirow
\usepackage{graphicx}
\usepackage{multirow}

% Appendix package
\usepackage[toc,page]{appendix}

\usepackage{fancyhdr}
\setlength{\headheight}{15.2pt}
\pagestyle{fancy}
\fancyhead[L]{\nouppercase{\leftmark}}
\fancyhead[R]{}
\renewcommand{\footrulewidth}{0.4pt}

% Row number command
\newcounter{rownr}
\newcommand{\rownumber}{\stepcounter{rownr}\arabic{rownr}}

%==============================================================================
% Start of document
%==============================================================================
\begin{document}

%------------------------------------------------------------------------------
% Title
%------------------------------------------------------------------------------
\include{cern-title}

%------------------------------------------------------------------------------
% Licensing info
%------------------------------------------------------------------------------
\pagebreak

\thispagestyle{empty}

\addcontentsline{toc}{section}{Licensing information}
\section*{Licensing information}

\noindent
This document is licensed under a Creative Commons Attribution-ShareAlike 4.0
International License. If you have not received a copy of the license along with this
work, see \\
\url{http://creativecommons.org/licenses/by-sa/4.0/}

%------------------------------------------------------------------------------
% Revision history
%------------------------------------------------------------------------------
\section*{Revision history}
\addcontentsline{toc}{section}{Revision history}

\centerline
{
  \rowcolors{2}{white}{gray!25}
  \begin{tabular}{l c p{.6\textwidth}}
  \hline
  \multicolumn{1}{c}{\textbf{Date}} & \multicolumn{1}{c}{\textbf{Version}} & \multicolumn{1}{c}{\textbf{Change}} \\
  \hline
  15-01-2015 & 0.1 & First draft \\
  \hline
  \end{tabular}
}

%------------------------------------------------------------------------------
% Table of contents, list of figs, tables
%------------------------------------------------------------------------------
\pagebreak
\pdfbookmark[1]{\contentsname}{toc}
\tableofcontents

%------------------------------------------------------------------------------
% List of abbreviations
%------------------------------------------------------------------------------
\pagebreak
\section*{List of abbreviations}
\begin{tabular}{l l}
FM & Front Module \\
PTS & Production Test Suite \\
RTM & Rear-Transition Module \\
\end{tabular}
\addcontentsline{toc}{section}{List of abbreviations}

%==============================================================================
% SEC: Intro
%==============================================================================
\pagebreak
\section{Introduction}
\label{sec:intro}

This document shows how to install a PTS test system for the CONV-TTL-BLO FM cards.
It takes the reader through setting up the 19'' rack for the CONV-TTL-BLO PTS and then
focuses on installing the test system (scripts, preparing shortcuts, etc.) on the PTS laptop.

%==============================================================================
% SEC: Rack setup
%==============================================================================
\section{Rack setup}
\label{sec:rack}

To setup the rack for the PTS system, we will need to insert an RTM Interface Tester
card into the back of the ELMA crate placed inside the 19'' rack. You will need
an RTM Interface Tester card to set up the system, so this would be a good time to
obtain one.

The following steps should be performed:

\begin{enumerate}
  \item Open the back of the 19'' rack
  \item Take the RTM Interface Tester card
  \item Plug it firmly into the P2 connector of slot 3 (or whichever slot you are building the system for)
  at the back of the ELMA crate
  \item Close the back of the 19'' rack
\end{enumerate}

%==============================================================================
% SEC: Install Ubuntu on the laptop
%==============================================================================
\section{Install Ubuntu on the laptop}
\label{sec:install-ubuntu}

If you end up with a bare laptop without an OS installed, you have to follow the
laptop installation guide written by Julian for the SVEC PTS system:

  \begin{itemize}
    \item \url{http://cern.ch/go/H9tv}
  \end{itemize}

Note that this guide can be followed selectively, for example you need not set up your
system to gain remote access to the laptop, the installation of the system can also be done
on the laptop directly. However, the steps related to network drivers and configuration
should be followed in order to have a properly working PTS system.

Also note that the admin account should be named \verb=pts-administrator=. This account
 name is important as it is embedded in the PTS scripts (Password \verb=pts-admin=).

The guide should be followed until \textbf{step 17} of section \textbf{Set up the PTS user account}.
After this step, the guide is SVEC-related and need not be followed. We will continue with
the details in the next section to test our system and network was properly installed.

%==============================================================================
% SEC: ELMA setup
%==============================================================================
\section{Setting up the ELMA crate}
\label{sec:elma-setup}

\begin{enumerate}
  \item The ELMA crate should be configured to have the password \verb=ADMIN= for the \verb=admin= account
  \item Login to the ELMA crate to test the setup in Section~\ref{sec:install-ubuntu}:
  \begin{verbatim}
  $ telnet elma
  Trying 192.168.20.22...
  Connected to elma.
  Escape character is '^]'.
  login:admin
  password: ADMIN
  %> 
	$
  \end{verbatim}
  \vspace{11pt}
  \item Make sure the ELMA crate has gateware version 2.31 or higher on it. In the SysMon Telnet prompt:
  \begin{verbatim}
  %>version
   Sysmon3Gen  041-452 Ind.A
   Software version 2.31 CERN
   Settings Saved in Internal FLASH
   MAC Address: FF:FF:FF:FF:FF:FF
   SN: 12345678
   Assembly ID: 0
  \end{verbatim}
  \item Now we need to disable the telnet ping that the SysMon does, so it doesn't get in the way of
  our Python scripts, which use Telnet to talk to the CONV-TTL-BLO FPGA. Run the following commands
  in the Telnet prompt of the ELMA crate:
  \begin{verbatim}
  telnetping 0
  saveenv
  reboot
  \end{verbatim}
  \item This last command will reboot the SysMon to store the telnetping settings, so the connection to the
  ELMA will be lost. This is an expected behaviour and should not get in our way. Simply reconnect
  as in step 2 to make sure the connection still works after reboot.
\end{enumerate}

%==============================================================================
% SEC: Test environment
%==============================================================================
\section{Preparing the laptop test environment}
\label{sec:environment}

After Ubuntu has been set up and the ELMA connection tested, we need to set up the
actual test scripts and a way of running them. This entails copying the PTS folder
structure for the CONV-TTL-BLO PTS to the laptop, copying the shortcuts to the
Desktop so that a user can run them and creating a dedicated Ubuntu user for the
CONV-TTL-BLO PTS.

Note that there are two ways of obtaining the PTS folder structure:

\begin{itemize}
  \item Download the ready-made archive from OHWR
  \item Build the test system from the repository
\end{itemize}

We will detail here the simplest of the two, downloading the ready-made archive.
See Appendix~\ref{app:build} for details on the second.

\begin{enumerate}
  \item Log in to the \verb=pts-administrator= user on the laptop
  \item Open up a terminal window, download the PTS folder structure tarball from OHWR
  to the home folder of the \verb=pts= user and extract the archive:
  \begin{verbatim}
  cd ~
  wget https://www.ohwr.org/attachments/5822/ubuntu.tar.gz
  tar xzvf ubuntu.tar.gz
  \end{verbatim}
  Note that you can also copy the archive from a USB stick instead of downloading it from OHWR.
  \item Check that all extracted files and folders have write permissions:
  \begin{small}
  \begin{verbatim}
  $ cd ubuntu/ttlbl/
  $ ls -l
  total 56
  drwxrwxr-x 7 tstana tstana  4096 Jan  8 11:45 ./
  drwxrwxr-x 3 tstana tstana  4096 Jan  8 11:45 ../
  drwxrwxr-x 2 tstana tstana  4096 Jan  8 11:45 boot/
  drwxrwxr-x 3 tstana tstana  4096 Jan  8 11:45 config/
  lrwxrwxrwx 1 tstana tstana    17 Jan  8 11:45 flashtest.py -> pyts/flashtest.py
  -rwxrwxr-x 1 tstana tstana 10933 Jan  8 11:45 jpts*
  drwxrwxr-x 2 tstana tstana  4096 Jan  8 11:45 log/
  -rwxrwxr-x 1 tstana tstana  2949 Jan  8 11:45 one*
  -rwxrwxr-x 1 tstana tstana  9906 Jan  8 11:45 pts*
  drwxrwxr-x 2 tstana tstana  4096 Jan  8 11:45 pyts/
  drwxrwxr-x 2 tstana tstana  4096 Jan  8 11:45 shell/
  lrwxrwxrwx 1 tstana tstana    20 Jan  8 11:45 test01.py -> pyts/dac_vcxo_pll.py
  lrwxrwxrwx 1 tstana tstana    12 Jan  8 11:45 test02.py -> pyts/leds.py
  lrwxrwxrwx 1 tstana tstana    24 Jan  8 11:45 test03.py -> pyts/ttl_pulse_switch.py
  lrwxrwxrwx 1 tstana tstana    23 Jan  8 11:45 test04.py -> pyts/blo_pulse_rtm.py
  lrwxrwxrwx 1 tstana tstana    16 Jan  8 11:45 test05.py -> pyts/therm_id.py
  lrwxrwxrwx 1 tstana tstana    18 Jan  8 11:45 test06.py -> pyts/sfp_eeprom.py
  lrwxrwxrwx 1 tstana tstana    16 Jan  8 11:45 test07.py -> pyts/sfp_test.py
  \end{verbatim}
  \end{small}
  \item If they do not have write permissions, as in the case above, make sure they do:
  \begin{verbatim}
  chmod a+w *
  \end{verbatim}
  \item Now  that we have the test scripts on the system we can create a dedicated
  user that will be used to run the PTS for the CONV-TTL-BLO. Open up Ubuntu's
  \textbf{User Accounts} window (click the \verb=pts= user account name at the top-right
  side of the screen), unlock this window by providing the \verb=pts= user account's
  password (\verb=pts-user=) and create a new user with the following settings:
  \begin{itemize}
    \item \textbf{Account Type:} Standard
    \item \textbf{Full name:} conv-ttl-blo
    \item \textbf{Username:} conv-ttl-blo
  \end{itemize}
  \item While still in the \textbf{User Accounts} window, click the new 
  \verb=conv-ttl-blo= user account
  \item Under \textbf{Login options}, click the \textbf{Password} dialog which should 
  at this moment be set to \textit{Account disabled}
  \item In the dialog that opens up, set the following parameters:
  \begin{itemize}
    \item \textbf{Action: } Set a password now
    \item \textbf{New password: } pts-user
  \end{itemize}
  \item Close the \textbf{User accounts} window
  \item Log in to the new \verb=conv-ttl-blo= user account by clicking on the \verb=pts= user
  on the top-right side of the screen and selecting the \verb=conv-ttl-blo= user
  \item Open a terminal and run the following commands to set up the PTS test system terminal
  windows and to copy the desktop shortcuts users will use to run the PTS:
  \begin{verbatim}
  cd ~
  cp .bashrc .bashrc.old
  cp /home/pts-administrator/ubuntu/ttlbl/config/bashrc.pts .bashrc
  cd ~/Desktop/
  cp /home/pts-administrator/ubuntu/ttlbl/config/*.desktop .
  \end{verbatim}
  \item Right-click on the terminal icon in the launcher bar at the left and click \textbf{Lock to Launcher}
  \item Close the terminal window
  \item Unlock all items except the folder navigator and the terminal from the launcher bar on the
  left side of the screen (right click the item and select \textbf{Unlock from Launcher})
  \item Set the desktop background for the user:
  \begin{itemize}
    \item on the Desktop, right-click and select \textbf{Change Desktop Background}
    \item hit the \textbf{+} under Wallpapers
    \item navigate to \verb=/home/pts-administrator/ubuntu/ttlbl/config/=
    \item select \verb=ttlbl-background.png= as the background image
    \item make sure the scaling of the image is set to \textbf{Zoom}
  \end{itemize}
  \item Finally, order the desktop launcher icons as shown in Figure~\ref{fig:icons}
  \item You should now have a working PTS test system. Navigate to the desktop and
  try to run the new \textbf{Run CONV-TTL-BLO PTS} executable here. Plug in a card
  to see if the PTS system communicates to it (no need to set up daisy-chains or anything
  for this test, you can do that later).
\end{enumerate}

\begin{figure}[h]
  \centerline{\includegraphics[scale=.44]{fig/icons.png}}
  \caption{\label{fig:icons} Order of PTS icons on the desktop}
\end{figure}

%==============================================================================
\pagebreak
\begin{appendices}
%==============================================================================

\section{Building the test system from the repository}
\label{app:build}

The CONV-TTL-BLO test system is devised to be built using a simple \verb=make=
command. The system contains a Makefile which, based on a few PTS-specific options,
builds up a folder structure for the PTS as the designer wants it. The \verb=make=
options for the PTS are:

\begin{itemize}
  \item \textbf{BOARD} -- this option specifies what is the starting string of the PTS log files (in the
  case of CONV-TTL-BLO: \verb=ttbl-*.log=, \verb=ttlbl-*.inf= )
  \item \textbf{ELMAIP} -- the IP or hostname of the ELMA crate the PTS is built for
  \item \textbf{ELMAPWD} -- the password for the admin account of the ELMA crate
  the PTS is built for
  \item \textbf{ELMASLOT} -- the slot number where the CONT-TTL-BLO card is placed during testing
\end{itemize}

As an example, the PTS folder structure for the CONV-TTL-BLO downloaded in Section~\ref{sec:environment}
was built using the following command:

\begin{verbatim}
  make ELMAIP=elma ELMAPWD=ADMIN ELMASLOT=3
\end{verbatim}

This built a system that communicates with the ELMA crate with the hostname \verb=elma= that
has the blocking pulse repeaters placed in slot 3 when the PTS is run.

In order to build a test system from scratch, follow these steps:

\begin{enumerate}
  \item Clone the repository for the CONV-TTL-BLO (\verb=git= is needed for this):
  \begin{small}
  \begin{verbatim}
  git clone git://ohwr.org/level-conversion/conv-ttl-blo/conv-ttl-blo-tst.git
  \end{verbatim}
  \end{small}
  \item Run \verb=make= in the \verb=fm/pts/= folder, setting the options as appropriate
  in your case:
  \begin{verbatim}
  cd conv-ttl-blo-tst/fm/pts
  make # ... set options here ...
  \end{verbatim}
\item Copy the entire \verb=./ubuntu= folder to the \verb=pts-administrator= home directory. Type:
	\begin{verbatim}
  cp -r ./ubuntu /home/pts-administrator/
  \end{verbatim}
\end{enumerate}

This will generate an archive similar to that downloaded in Section~\ref{sec:environment},
that should be copied to the test system as described in Section~\ref{sec:environment}.

%==============================================================================
\end{appendices}
%==============================================================================

\end{document}
