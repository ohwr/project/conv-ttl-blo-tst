%==============================================================================
% Document header
%==============================================================================
\documentclass[a4paper,11pt]{article}

% Hyperrefs
\usepackage[
  colorlinks = true,
  linkcolor  = black,
  citecolor  = black,
  urlcolor   = blue,
]{hyperref}

\usepackage{graphicx}
\usepackage{multirow}

% Color package
\usepackage[usenames,dvipsnames,table]{xcolor}

\usepackage[toc,page]{appendix}

% Header and footer customization
\usepackage{fancyhdr}
\setlength{\headheight}{15.2pt}
\pagestyle{fancy}
\fancyhead[L]{\nouppercase{\leftmark}}
\fancyhead[R]{} 
\renewcommand{\footrulewidth}{0.4pt}

%==============================================================================
% Start of document
%==============================================================================
\begin{document}

%------------------------------------------------------------------------------
% Title
%------------------------------------------------------------------------------
\include{cern-title}

%------------------------------------------------------------------------------
% Licensing info
%------------------------------------------------------------------------------
\pagebreak

\thispagestyle{empty}

\addcontentsline{toc}{section}{Licensing information}
\section*{Licensing information}

\noindent
This document is licensed under a Creative Commons Attribution-ShareAlike 4.0
International License. If you have not received a copy of the license along with this
work, see \\
\url{http://creativecommons.org/licenses/by-sa/4.0/}

%------------------------------------------------------------------------------
% Revision history
%------------------------------------------------------------------------------
\section*{Revision history}
\addcontentsline{toc}{section}{Revision history}

\centerline
{
  \rowcolors{2}{white}{gray!25}
  \begin{tabular}{l c p{.6\textwidth}}
  \hline
  \multicolumn{1}{c}{\textbf{Date}} & \multicolumn{1}{c}{\textbf{Version}} & \multicolumn{1}{c}{\textbf{Change}} \\
  \hline
  01-07-2013 & 0.1 & First draft \\
  07-08-2013 & 0.2 & Added additional documentation \\
  16-01-2015 & 1.0 & Updates to document structure, updated links in the references section,
                     removed the appendix (schematics can be found online), corrected the
                     connection diagram (Table~\ref{tbl:rtm-sigs}) per the schematics and 
                     updated Figure~\ref{fig:loopback} \\
  \hline
  \end{tabular}
}

%------------------------------------------------------------------------------
% Table of contents, list of figs, tables
%------------------------------------------------------------------------------
\pagebreak
\pdfbookmark[1]{\contentsname}{toc}
\tableofcontents

\listoffigures
\listoftables

%------------------------------------------------------------------------------
% List of abbreviations
%------------------------------------------------------------------------------
\section*{List of abbreviations}
\begin{tabular}{l l}
PTS & Production Test Suite \\
RTM & Rear Transition Module \\
\end{tabular}

\pagebreak
\pagenumbering{arabic}
\setcounter{page}{1}

%==============================================================================
% SEC: Intro
%==============================================================================
\section{Introduction}
\label{sec:intro}

Production Test Suite (PTS) is the environment designed for the functionality tests of
boards at the manufacturing site, right after production. It assures that boards comply
with a minimum set of quality rules in terms of soldering, mounting and PCB fabrication
process.

This document contains information about the hardware of the CONV-TTL-BLO PTS.

%------------------------------------------------------------------------------
% SUBSEC: Additional docs
%------------------------------------------------------------------------------
\subsection*{Additional documentation}

\begin{itemize}
  \item CONV-TTL-BLO User Guide \cite{ctb-ug}
  \item CONV-TTL-BLO Hardware Guide \cite{ctb-hwguide}
  \item CONV-TTL-BLO HDL Guide \cite{ctb-hdlguide}
  \item CONV-TTL-BLO PTS User Guide \cite{pts-userguide}
  \item CONV-TTL-BLO PTS HDL Guide \cite{pts-hdlguide}
\end{itemize}


%======================================================================================
% SEC: PTS system
%======================================================================================
\section{PTS system}
\label{sec:pts-sys}

The PTS system (Figure~\ref{fig:pts-sys}) is contained within a rack containing an ELMA
crate, the laptop with the PTS software installed on it and all other accessories necessary
for running tests. More information about the PTS system can be found in \cite{pts-userguide}.

\begin{figure}[h]
  \centerline{\includegraphics[width=\textwidth]{fig/pts-sys}}
  \caption{PTS System}
  \label{fig:pts-sys}
\end{figure}

The ELMA crate within the PTS contains a system monitor (SysMon) board that monitors
voltage levels, temperatures and controls fan speeds for the crate fans. The crate
can be accessed via Telnet to send commands to the VME boards inside the crate.

The CONV-TTL-BLO is placed on a VME slot within the ELMA crate. The PTS firmware
uses hardware on the CONV-TTL-BLO board to test the board itself. The SysMon will
control the CONV-TTL-BLO via the I$^2$C lines on the VME P1 connector to run the tests.

An RTM Interface Tester board is the only extra hardware needed for the PTS. This
board is the subject of the next section. 


%======================================================================================
% SEC: PTS system
%======================================================================================
\section{RTM Interface Tester}
\label{sec:rtm-tester}

The RTM Interface Tester board is a simple board used to loop back signals relevant in
the blocking pulse and RTM interface test. It is placed on the P2 connector in the VME
back-plane and it helps test
\begin{itemize}
  \item that a minimum of 16~V are output by the CONV-TTL-BLO on the blocking outputs
  \item the RTM detection lines
  \item the rear-panel pulse LED lines are working properly
\end{itemize}

\textbf{\textit{Pay attention when using a general test system that the CONV-TTL-BLO RTM
Interface Tester is not plugged in when testing e.g., SVEC boards. If the SVEC
board should drive the wrong lines high, this might cause a permanent fault to
the SVEC AFPGA.}}

Table~\ref{tbl:rtm-sigs} lists the output-to-input connections made by the RTM Interface Tester
on the blocking side of CONV-TTL-BLO.

\begin{table}[h]
  \label{tbl:rtm-sigs}
  \caption{Output-to-input connections on a six-channel RTM}
  \centerline
  {
    \begin{tabular}{c c c c c}
    \hline
    \textbf{Board} & \textbf{Output} & \multicolumn{3}{c}{\textbf{Input}} \\
    \hline
    \multirow{3}{*}{1} & O1 & I1 & I2 & I3 \\
                       & O2 & I2 & I3 & I1 \\
                       & O3 & I3 & I1 & I2 \\
    \hline
    \multirow{3}{*}{2} & O4 & I4 & I5 & I6 \\
                       & O5 & I5 & I6 & I4 \\
                       & O6 & I6 & I5 & I4 \\
    \hline
    \end{tabular}
  }
\end{table}

\begin{figure}[h]
  \centerline{\includegraphics[width=.7\textwidth]{fig/loopback}}
  \caption{Loopback voltage divider diagram}
  \label{fig:loopback}
\end{figure}

Before looping it back to an input channel, the output blocking signal from the
CONV-TTL-BLO is passed through a voltage divider network (Figure~\ref{fig:loopback}).
The 150~$\Omega$ equivalent resistor forms a 1/4 voltage divider with the 50~$\Omega$
termination at the blocking input. Given the 4~V sensitivity of the blocking input,
this means that any signals lower than about 16~V at the blocking output of the
CONV-TTL-BLO will not be detected back at the input.

By looping back the rear-panel LED outputs onto the RTM detection lines, the rest
of the RTM interface is tested.

The full schematics of the RTM Interface Tester can be found here:

\noindent
\begin{footnotesize}
\url{http://www.ohwr.org/attachments/download/3703/RTM_Interface_Tester_sch.pdf}
\end{footnotesize}

%==============================================================================
% Bibliography
%==============================================================================
\pagebreak
\bibliographystyle{ieeetr}
\bibliography{hwg-pts-conv-ttl-blo}
\addcontentsline{toc}{section}{References}

\end{document}
