target = "xilinx"
action = "synthesis"
syn_device  = "xc6slx45t"
syn_grade   = "-3"
syn_package = "fgg484"
syn_top     = "pts"
syn_project = "pts.xise"

modules = {
    "local" : [
      "../top"
      ]
    }
