##_______________________________________________________________________________________________
##
##                                        CONV-TTL-BLO PTS
##
##                                         CERN,BE/CO-HT
##_______________________________________________________________________________________________
##
##-----------------------------------------------------------------------------------------------
##
##                                    CONV-TTL-BLO pcb version
##
##-----------------------------------------------------------------------------------------------
##
## Description  Test whether the version of PCB is the expected one
##
##
## Authors      Maciej Lipinski (maciej.lipinski@cern.ch)
## Website      http://www.ohwr.org/projects/pts
## Date         17/08/2017
##-----------------------------------------------------------------------------------------------
##
##------------------------------------------------------------------------------------------------
##                               GNU LESSER GENERAL PUBLIC LICENSE
##                              ------------------------------------
## This source file is free software; you can redistribute it and/or modify it under the terms of
## the GNU Lesser General Public License as published by the Free Software Foundation; either
## version 2.1 of the License, or (at your option) any later version.
## This source is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
## without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
## See the GNU Lesser General Public License for more details.
## You should have received a copy of the GNU Lesser General Public License along with this
## source; if not, download it from http://www.gnu.org/licenses/lgpl-2.1.html
##-------------------------------------------------------------------------------------------------

##-------------------------------------------------------------------------------------------------
##                                            Import
##-------------------------------------------------------------------------------------------------

# Import system modules
import sys
sys.path.append("log/")
import time
import os, errno, re, sys, struct
import os.path
import traceback
import glob
import binascii

# Import common modules
from ctypes import *
from ptsexcept import *
from vv_pts import *
from ptsdefine import *

##-------------------------------------------------------------------------------------------------
##                                             main                                              --
##-------------------------------------------------------------------------------------------------

def main(bus,tname,inf,log):

    """
    tests : Hardware version
    uses  : pts.bit and hwvertest.py
    """

    HWVERS = 4.1

    pel = PTS_ERROR_LOGGER(inf,log)
    
    try:

        # Read PCB version: a 6 bits representing HW/PCB version number 
        # 4 MSB represent HW version number   (major)
        # 2 LSB represent number of execution (minor)
        # Eg: value 010010 represents PCB version 4.2
        hwvers = (bus.vv_read(CSR) & 0x3F00) >> CSR_HWVERS_OFS
        
        maj = int(hwvers >> 2)
        min = float(hwvers & 0x03)
        min /= 10
        hwvers = maj + min

        # and now check if appropriate
        if (hwvers == HWVERS):
            msg = "HW/PCB version correct: %2.1f\n" % (hwvers)
            inf.write(msg)
        else:
            msg = "ERROR: HW/PCBe version (%2.1f) incorrect - expected %2.1f" % (hwvers, HWVERS)
            pel.set(msg)

        print "-->%s" % msg

        return pel.get()

    except BusException, e:
        raise PtsError("SKT Exception: %s" % (e))
    
    except BusWarning, e:
        raise PtsError("SKT Warning: %s" % (e))
