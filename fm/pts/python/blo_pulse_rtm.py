##_______________________________________________________________________________________________
##
##                                        CONV-TTL-BLO PTS
##
##                                         CERN,BE/CO-HT
##_______________________________________________________________________________________________
##
##-----------------------------------------------------------------------------------------------
##
##                                CONV-TTL-BLO RTM interface test
##
##-----------------------------------------------------------------------------------------------
##
## Description  This module implements the blocking pulse repetition and RTM interface test. The
##              circuits tested here are the input blocking channels with the main active part,
##              the Avago ouptocoupler ICs (IC22-27) and input Schmitt trigger (IC16); the 18
##              blocking output channels, along with the T1-18 transistors, the Coilcraft
##              transformers L1-12, L17-22 and the 24V blocking power supply IC32 and reset
##              generator IC29; the IC10 enabling NAND gate and output buffer IC4; the Schmitt
##              trigger drivers and lines for the pulse LEDs on the RTM and the Schmitt trigger
##              inputs for the RTM detection lines.
##
##              The RTM Interface Tester board on the P2 connector should be used alongside this
##              module. It implements the necessary loopbacks that this module requires and the
##              voltage dividers necessary to test the IC32 power supply. The voltage level at
##              blocking output should be ~24V. This is divided (using the 50-ohm resistors at
##              the blocking input) by four; since input optocouplers are sensitive down to ~5V,
##              any voltage below 20V will not be received and is defined as an error.
##
##              Counters are implemented in the firmware to count the number of pulses sent and
##              received on a channel. There are two counters per channel, one for input and
##              one for output; the counters increment on the rising edge of a pulse signal. The
##              current value of each counter can be read via a special Wishbone-mapped slave,
##              accessible at base address 0xC00. Below is the full address map for the blocking
##              pulse counters:
##
##                 WB addr    SKT addr   Counter
##                  0xC50       789      BLO_CH1_OUT
##                  0xC54       790      BLO_CH1_IN
##                  0xC58       791      BLO_CH2_OUT
##                  0xC5C       792      BLO_CH2_IN
##                  0xC60       793      BLO_CH3_OUT
##                  0xC64       794      BLO_CH3_IN
##                  0xC68       795      BLO_CH4_OUT
##                  0xC6C       796      BLO_CH4_IN
##                  0xC70       797      BLO_CH5_OUT
##                  0xC74       798      BLO_CH5_IN
##                  0xC78       799      BLO_CH6_OUT
##                  0xC7C       800      BLO_CH6_IN
##
##              The Wishbone slave is the same used in the TTL pulse repeater tests; the blocking
##              channels occupy the addresses following those of the TTL counters.
##
##              The FPGA firmware implements one pulse generator per channel on the blocking
##              outputs configured to output 1-us long pulses at a delay of 100ms apart (CH1 will
##              output at 0ms, CH2 at 100 ms, CH3 at 200ms, etc.) and with a frequency of 600ms.
##              These pulses are looped back on the RTM Interface Tester and if of correct
##              amplitude, received at three different inputs, as shown below:
##
##                        Output            Inputs
##                          CH1         CH1, CH2, CH3
##                          CH2         CH2, CH3, CH4
##                          CH3         CH3, CH4, CH5
##                          CH4         CH4, CH5, CH6
##                          CH5         CH5, CH6, CH1
##                          CH6         CH6, CH1, CH2
##
##              This module contains a pulse counter class which implements methods to decode
##              channel pulse counter numbers into their respective address. In the main
##              function of the module, a pulse counter object is created and pulses are
##              generated for 600 ms. The test program waits for these 600ms, stops pulse
##              generation on CH10 and reads the pulse counter values. If the number of pulses
##              received on ALL channels matches the expected number of pulses (plus a margin),
##              the test passes, otherwise the circuit that might be the fault is indicated.
##
##              In order to test the rest of the RTM interface (pulse LEDs, RTM identification),
##              the pulse LED lines are looped back to the RTM lines. When pulse generation is
##              stopped, all-ones are written by the firmware on the pulse LED lines. If the
##              circuit works, they should be read back as all-ones on the RTM identification
##              lines.
##
## Authors      Julian Lewis (Julian.Lewis@cern.ch)
##              Theodor-Adrian Stana (t.stana@cern.ch)
## Website      http://www.ohwr.org/projects/conv-ttl-blo
## Date         30/04/2013
##-----------------------------------------------------------------------------------------------
##
##------------------------------------------------------------------------------------------------
##                               GNU LESSER GENERAL PUBLIC LICENSE
##                              ------------------------------------
## This source file is free software; you can redistribute it and/or modify it under the terms of
## the GNU Lesser General Public License as published by the Free Software Foundation; either
## version 2.1 of the License, or (at your option) any later version.
## This source is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
## without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
## See the GNU Lesser General Public License for more details.
## You should have received a copy of the GNU Lesser General Public License along with this
## source; if not, download it from http://www.gnu.org/licenses/lgpl-2.1.html
##-------------------------------------------------------------------------------------------------

##-------------------------------------------------------------------------------------------------
##                                            Import
##-------------------------------------------------------------------------------------------------

# Import system modules
import sys
sys.path.append("pyts")
import time
import os, errno, re, sys, struct
import os.path
import traceback

# Import common modules
from ctypes import *
from ptsexcept import *
from vv_pts import *
from ptsdefine import *

class CPulseCounter:

    def __init__(self, bus, base):
        self.bus  = bus
        self.base = base

    def wr_reg(self, addr, val):
        self.bus.vv_write(self.base + addr,val)

    def rd_reg(self, addr):
        return self.bus.vv_read(self.base + addr)

    def rd_out_cnt(self, chan):
        return self.rd_reg((chan-1)*8)

    def rd_in_cnt(self, chan):
        return self.rd_reg((chan-1)*8 + 4)

##-------------------------------------------------------------------------------------------------
##                                             main                                              --
##-------------------------------------------------------------------------------------------------
def main(bus, tname, inf, log):

    """
    tests : Blocking pulse repetition; input optocouplers IC22-IC27
            and Schmitt trigger buffer IC16; output buffer IC4, NAND
            gate IC10; blocking output transistors T1-T18 and
            transformers L1-L12, L17-L22; reset generator IC29 and
            blocking power supply generator IC32; Schmitt triggers
            IC21, IC28
    uses  : pts.bit and blo_pulse_rtm.py
    """

    pel = PTS_ERROR_LOGGER(inf, log)

    try:
        # Initialize a pulse counter object
        pc = CPulseCounter(bus, PULSE_CNT_BASE)

        # Enable pulse generation while keeping the status LEDs sequencing
        val = bus.vv_read(CSR)
        val |= (1 << CSR_REARPT_OFS)
        bus.vv_write(CSR, val)

        # Stop pulse generation after 1.7 seconds - with 600ms pulse period, that should make about 3 pulses
        time.sleep(1.7)
        val &= ~(1 << CSR_REARPT_OFS)
        bus.vv_write(CSR, val)

        # Read the channel registers, blocking channels 1-6 correspond to pulse
        # counter channels 11-16
        ic_arr = []
        oc_arr = []
        for i in range(11,17):
            ic_arr.append(pc.rd_in_cnt(i))
            oc_arr.append(pc.rd_out_cnt(i))

        # First, check for all-zeroes in input counters, indicating enable NAND gate or blocking power supply failure
        if all(ic == 0 for ic in ic_arr):
            msg = "ERROR: No pulses received (or transmitted), check enable NAND gate IC10 and blocking power supply IC32"
            pel.set(msg)

        # Compute theoretical ic_arr based on oc_arr
        ic_sim = []
        for i in range(0,len(oc_arr)):
            ic_sim.append(oc_arr[i]+oc_arr[i-1]+oc_arr[i-2])

        # Check if the right number of pulses have been received
        for i in range(0,len(ic_arr)):
            if (ic_arr[i] != 0) and (ic_arr[i] == ic_sim[i]):
                msg = "CH%d sent %d pulse(s) and received %d pulse(s) - good\n" % (i+1, oc_arr[i], ic_arr[i])
                inf.write(msg)
            else:
                msg = "ERROR: CH%d sent %d pulses and received %d pulses - expected %d" % (i+1, oc_arr[i], ic_arr[i], ic_sim[i])
                pel.set(msg)

        # Rear LED lines test - set the RLEDT bit in the CSR, which triggers setting all LED lines high.
        # The LED lines are looped back to the RTM lines, which are read to validate proper operation.
        val |= (1 << CSR_RLEDT_OFS)
        rtm = (bus.vv_read(CSR) >> CSR_RTM_OFS) & 0x3f
        if (rtm == 0x3f):
            msg = "RTM lines have expected value (0x%02X)" % rtm
            inf.write(msg)
        else:
            msg = "ERROR: RTM lines have unexpected value (0x%02X) - check IC21, IC28" % rtm
            pel.set(msg)

        inf.write("\n")

        # Finally, return the number of errors that occured
        return pel.get()

    except BusException, e:
        raise PtsError("SKT Exception: %s" % e)

    except BusWarning, e:
        raise PtsError("SKT Warning: %s" % e)

    finally:
        val = bus.vv_read(CSR)
        val &= ~(1 << CSR_REARPT_OFS)
        val &= ~(1 << CSR_RLEDT_OFS)
        bus.vv_write(CSR, val)

