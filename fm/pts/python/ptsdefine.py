#===============================================================================
# CERN (BE-CO-HT)
# PTS definitions file
#===============================================================================
# author: Theodor Stana (t.stana@cern.ch)
#
# date of creation: 2013-10-31
#
# version: 1.0
#
# description:
#     This module contains register address definitions that are used across the
#     various tests. Importing this module inside a test script makes these
#     definitions available for use within a bus.vv_write or bus.vv_read method
#     (see vv_pts.py for these methods).
#
# dependencies:
#     none.
#
# references:
#     none.
#
#===============================================================================
# GNU LESSER GENERAL PUBLIC LICENSE
#===============================================================================
# This source file is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation; either version 2.1 of the License, or (at your
# option) any later version. This source is distributed in the hope that it
# will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Lesser General Public License for more details. You should have
# received a copy of the GNU Lesser General Public License along with this
# source; if not, download it from http://www.gnu.org/licenses/lgpl-2.1.html
#===============================================================================
# last changes:
#    2014-10-31    Theodor Stana     t.stana@cern.ch     File created
#===============================================================================
#  TODO: -
#===============================================================================

BOARD = "$BOARD"

# ELMA crate definitions
ELMAIP = "$ELMAIP"
ELMAPWD = "$ELMAPWD"
ELMASLOT = $ELMASLOT

# Board ID register
BIDR = 0x000

BIDR_ARR = [ "TBLO", "T485" ]

# Control and Status Register
CSR                = 0x004
CSR_CHLEDT_OFS     = 0
CSR_STLEDT_OFS     = 1
CSR_RLEDT_OFS      = 2
CSR_TTLPT_OFS      = 3
CSR_REARPT_OFS     = 4
CSR_HWVERS_OFS     = 8
CSR_RST_UNLOCK_OFS = 14
CSR_RST_OFS        = 15
CSR_SWITCH_OFS     = 16
CSR_RTM_OFS        = 24
CSR_I2C_ERR_OFS    = 30
CSR_I2C_WDTO_OFS   = 31

# Line Status Register
LSR              = 0x008
LSR_FRONT_OFS    = 0
LSR_FRONTINV_OFS = 6
LSR_REAR_OFS     = 10


# 1-Wire base address, used in therm_id.py
TEMP_1WIRE_BASE      = 0x010

# 1-Wire unique ID
UIDREGLS = 0xac		# 1-wire chip Unique ID - LSB
UIDREGMS = 0xb0		# 1-wire chip Unique ID - MSB


# DAC and clock info registers and offsets, used in dac_vcxo_pll.py
PLL_DAC_BASE         = 0X020
VCXO_DAC_BASE        = 0x080

PLL_CLKINFO_BASE     = 0x100
VCXO_CLKINFO_BASE    = 0x120
CLKINFO_RST_OFS      = 0x014
CLKINFO_ENABLE_OFS   = 0x018
CLKINFO_VALUE_OFS    = 0x010

# SFP I2C master base address, used in sfp_eeprom.py
SFP_EEPROM_BASE      = 0x140

# SFP endpoint, miniNIC and buffer RAM base addresses and offsets,
# used in sfp_test.py
SFP_BASE             = 0x200
SFP_ENDPOINT_OFS     = 0x000
SFP_MINIC_OFS        = 0x200
SFP_DPRAM_OFS        = 0x600

# Pulse counter base address
PULSE_CNT_BASE       = 0xc00
