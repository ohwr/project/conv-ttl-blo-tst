##_______________________________________________________________________________________________
##
##                                        CONV-TTL-BLO PTS
##
##                                         CERN,BE/CO-HT
##_______________________________________________________________________________________________
##
##-----------------------------------------------------------------------------------------------
##
##                                     CONV-TTL-BLO LEDs test
##
##-----------------------------------------------------------------------------------------------
##
## Description  Testing of the Front Panel status LEDs (IC1 and bi-color LEDs) and pulse
##              LEDs (IC5 and channel LEDs) of the CONV-TTL-BLO board.
##
##              For this test, the operator's intervention is needed; when prompted, the operator
##              needs to visually check the LEDS.
##
##              The FPGA firmware sequences through each of the front panel LEDs (pulse LEDs
##              status LEDs) based on the value of the current test field in the PTS control
##              register. In order to start and stop LED sequencing, the test sets and clears
##              control bits in the PTS CSR. Bit 3 of CSR enables pulse LED sequencing and
##              bit 4 enables status LED sequencing.
##
## Authors      Julian Lewis (Julian.Lewis@cern.ch)
##              Theodor-Adrian Stana (t.stana@cern.ch)
## Website      http://www.ohwr.org/projects/pts
## Date         12/04/2013
##-----------------------------------------------------------------------------------------------
##
##------------------------------------------------------------------------------------------------
##                               GNU LESSER GENERAL PUBLIC LICENSE
##                              ------------------------------------
## This source file is free software; you can redistribute it and/or modify it under the terms of
## the GNU Lesser General Public License as published by the Free Software Foundation; either
## version 2.1 of the License, or (at your option) any later version.
## This source is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
## without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
## See the GNU Lesser General Public License for more details.
## You should have received a copy of the GNU Lesser General Public License along with this
## source; if not, download it from http://www.gnu.org/licenses/lgpl-2.1.html
##-------------------------------------------------------------------------------------------------

##-------------------------------------------------------------------------------------------------
##                                            Import
##-------------------------------------------------------------------------------------------------

# Import system modules
import sys
sys.path.append("pyts")
import time
import os, errno, re, sys, struct
import os.path
import traceback

# Import common modules
from ctypes import *
from ptsexcept import *
from vv_pts import *
from ptsdefine import *

##-------------------------------------------------------------------------------------------------
##                                             main                                              --
##-------------------------------------------------------------------------------------------------

def main(bus,tname,inf,log):

    """
    tests : Front Panel LEDs with Schmitt trigger driver IC5, bicolor LEDs with transceiver IC1
    uses  : pts.bit and leds.py
    """

    pel = PTS_ERROR_LOGGER(inf,log)

    try:

    # First phase of LED test, enable pulse LED sequencing
        bus.vv_write(CSR, 1 << CSR_CHLEDT_OFS)

        # The firmware should blink the LEDs, ask the operator for input
        inp = raw_input("--> Are the channel LEDs blinking one by one? yes/no: ")
        while True:
            if inp.find("yes") != -1 or inp.find("YES") != -1:
                break

            if inp.find("no") != -1 or inp.find("NO") != -1:
               msg = "ERROR: Front Panel LEDs or Schmitt trigger IC5"
               pel.set(msg)
               break

            inp = raw_input('Please type "yes" or "no" to continue:')

    # Second phase of LED test, enable status LED sequencing
        bus.vv_write(CSR, 1 << CSR_STLEDT_OFS)

        # The firmware should blink the LEDs, ask the operator for input
        inp = raw_input("--> Are the status LEDs blinking one by one?  yes/no: ")
        while True:
            if inp.find("yes") != -1 or inp.find("YES") != -1:
                break

            if inp.find("no") != -1 or inp.find("NO") != -1:
               msg = "ERROR: Status LEDs or transceiver IC1"
               pel.set(msg)
               break

            inp = raw_input('Please type "yes" or "no" to continue:')

        return pel.get()

    except BusException, e:
        raise PtsError("SKT Exception: %s" % (e))

    except BusWarning, e:
        raise PtsError("SKT Warning: %s" % (e))

    finally:
        # Clean-up -- clear status and front LED test bits
        val = bus.vv_read(CSR)
        val &= ~((1 << CSR_CHLEDT_OFS) | (1 << CSR_STLEDT_OFS))
        bus.vv_write(CSR, val)

