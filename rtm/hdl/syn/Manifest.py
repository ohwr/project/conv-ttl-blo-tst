target = "xilinx"
action = "synthesis"
syn_device  = "xc6slx45t"
syn_grade   = "-3"
syn_package = "fgg484"
syn_top     = "pts_conv_ttl_blo_rtm"
syn_project = "pts_conv_ttl_blo_rtm.xise"

modules = {
    "local" : [
        "../top/"
      ]
    }
