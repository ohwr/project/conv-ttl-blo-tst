--==============================================================================
-- CERN (BE-CO-HT)
-- Reset generator for CONV-TTL-* boards
--==============================================================================
--
-- author: Theodor Stana (t.stana@cern.ch)
--
-- date of creation: 2013-03-05
--
-- version: 1.0
--
-- description: 
--    This module generates a controllable-width reset pulse. The width of the
--    reset pulse is set via the g_reset_time pulse; an internal counter counts
--    up to this value and de-asserts the active-low reset line when the value
--    has been reached. At the same time, the module is de-activated.
--
--    By default, a 20 MHz clock (50 ns period) is assumed, resulting in a 100ms
--    reset width.
--
-- dependencies:
--    none
-- 
--==============================================================================
-- GNU LESSER GENERAL PUBLIC LICENSE
--==============================================================================
-- This source file is free software; you can redistribute it and/or modify it
-- under the terms of the GNU Lesser General Public License as published by the
-- Free Software Foundation; either version 2.1 of the License, or (at your
-- option) any later version. This source is distributed in the hope that it
-- will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
-- See the GNU Lesser General Public License for more details. You should have
-- received a copy of the GNU Lesser General Public License along with this
-- source; if not, download it from http://www.gnu.org/licenses/lgpl-2.1.html
--==============================================================================
-- last changes:
--    2013-03-05   Theodor Stana    File created
--==============================================================================
-- TODO: - 
--==============================================================================

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity reset_gen is
  generic
  (
    -- Reset time in number of clk_i cycles
    g_reset_time : positive := 2_000_000
  );
  port
  (
    clk_i   : in  std_logic;
    rst_i   : in  std_logic;
    rst_n_o : out std_logic
  );
end entity reset_gen;


architecture behav of reset_gen is

  --============================================================================
  -- Function and procedure declarations
  --============================================================================
  function f_log2_size (A : natural) return natural is
  begin
    for I in 1 to 64 loop               -- Works for up to 64 bits
      if (2**I >= A) then
        return(I);
      end if;
    end loop;
    return(63);
  end function f_log2_size;

  --============================================================================
  -- Signal declarations
  --============================================================================
  signal cnt    : unsigned(f_log2_size(g_reset_time)-1 downto 0) := (others => '0');
  signal cnt_en : std_logic := '1';

--==============================================================================
--  architecture begin
--==============================================================================
begin

  --============================================================================
  -- Reset generation logic
  --============================================================================
  p_rst_gen: process(clk_i)
  begin
    if rising_edge(clk_i) then
      if (rst_i = '1') then
        cnt_en  <= '1';
        cnt     <= (others => '0');
      elsif (cnt_en = '1') then
        rst_n_o <= '0';
        cnt     <= cnt + 1;
        if (cnt = g_reset_time-1) then
          rst_n_o <= '1';
          cnt_en  <= '0';
        end if;
      end if;
    end if;
  end process p_rst_gen;

end architecture behav;
--==============================================================================
--  architecture end
--==============================================================================
