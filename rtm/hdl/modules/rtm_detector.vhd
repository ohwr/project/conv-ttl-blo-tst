--==============================================================================
-- CERN (BE-CO-HT)
-- Rear transition module (RTM) detector
--==============================================================================
--
-- author: Carlos Gil Soriano
--         Theodor Stana (t.stana@cern.ch)
--
-- date of creation: 2013-01-09
--
-- version: 2.0
--
-- description: 
--
--   This module detects the presence of rear transition module motherboards
--   (RTMMs) and piggybacks (RTMPs). Detection works by checking the RTMM and 
--   RTMP input pins and these pins are pulled up on the front module. The 
--   RTMM_OK and RTMP_OK ouputs are set if the corresponding inputs do not
--   yield errors. Different boards have the RTMM/P pins setup differently, 
--   as outlined in the tables below:
--       
--          Table 1. RTMM detection pins.
--     __________________________________________
--     | Board     | RTMM[2] | RTMM[1] | RTMM[0] |
--     +-----------------------------------------+
--     | Error     |     '1' |     '1' |     '1' |
--     | RTMM_V1   |     '1' |     '1' |     '0' |
--     | RTMM_V2   |     '1' |     '0' |     '1' |
--     | Reserved  |     '1' |     '0' |     '0' |
--     | Reserved  |     '0' |     '1' |     '1' |
--     | Reserved  |     '0' |     '1' |     '0' |
--     | Reserved  |     '0' |     '0' |     '1' |
--     | Reserved  |     '0' |     '0' |     '0' |
--     +-----------+---------+---------+---------+
--
--
--          Table 2. RTMP detection pins.
--     _____________________________________________
--     | Board       | RTMP[2] | RTMP[1] | RTMP[0] |
--     +-------------------------------------------+
--     | Error OR    |     '1' |     '1' |     '1' |
--     | Blocking_V1 |         |         |         |
--     | RS485_V1    |     '1' |     '1' |     '0' |
--     | -Reserved-  |     '1' |     '0' |     '1' |
--     | -Reserved-  |     '1' |     '0' |     '0' |
--     | -Reserved-  |     '0' |     '1' |     '1' |
--     | -Reserved-  |     '0' |     '1' |     '0' |
--     | -Reserved-  |     '0' |     '0' |     '1' |
--     | Error       |     '0' |     '0' |     '0' |
--     +-------------+---------+---------+---------+
--
--
-- dependencies:
--   none
--
-- references:
--    http://www.ohwr.org/projects/conv-ttl-blo/wiki/RTM_board_detection
--
--==============================================================================
-- GNU LESSER GENERAL PUBLIC LICENSE
--==============================================================================
-- This source file is free software; you can redistribute it and/or modify it
-- under the terms of the GNU Lesser General Public License as published by the
-- Free Software Foundation; either version 2.1 of the License, or (at your
-- option) any later version. This source is distributed in the hope that it
-- will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
-- See the GNU Lesser General Public License for more details. You should have
-- received a copy of the GNU Lesser General Public License along with this
-- source; if not, download it from http://www.gnu.org/licenses/lgpl-2.1.html
--==============================================================================
-- last changes:
--    2013-01-09   Theodor Stana     t.stana@cern.ch     File created
--==============================================================================
-- TODO: - 
--==============================================================================

library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity rtm_detector is
  port 
  (
    rtmm_i    : in  std_logic_vector(2 downto 0);
    rtmp_i    : in  std_logic_vector(2 downto 0);
    rtmm_ok_o : out std_logic;
    rtmp_ok_o : out std_logic
  );
end entity rtm_detector;

architecture behav of rtm_detector is

--==============================================================================
--  architecture begin
--==============================================================================
begin

  rtmm_ok_o <= '0' when (rtmm_i = "111") else '1';
  rtmp_ok_o <= '0' when (rtmp_i = "111") else '1';

end behav;
--==============================================================================
--  architecture end
--==============================================================================
