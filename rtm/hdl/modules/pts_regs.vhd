---------------------------------------------------------------------------------------
-- Title          : Wishbone slave core for PTS control and status registers
---------------------------------------------------------------------------------------
-- File           : pts_regs.vhd
-- Author         : auto-generated by wbgen2 from pts_regs.wb
-- Created        : Thu Jan 16 16:51:51 2014
-- Standard       : VHDL'87
---------------------------------------------------------------------------------------
-- THIS FILE WAS GENERATED BY wbgen2 FROM SOURCE FILE pts_regs.wb
-- DO NOT HAND-EDIT UNLESS IT'S ABSOLUTELY NECESSARY!
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity pts_regs is
  port (
    rst_n_i                                  : in     std_logic;
    clk_sys_i                                : in     std_logic;
    wb_adr_i                                 : in     std_logic_vector(0 downto 0);
    wb_dat_i                                 : in     std_logic_vector(31 downto 0);
    wb_dat_o                                 : out    std_logic_vector(31 downto 0);
    wb_cyc_i                                 : in     std_logic;
    wb_sel_i                                 : in     std_logic_vector(3 downto 0);
    wb_stb_i                                 : in     std_logic;
    wb_we_i                                  : in     std_logic;
    wb_ack_o                                 : out    std_logic;
    wb_stall_o                               : out    std_logic;
-- Port for std_logic_vector field: 'bits' in reg: 'Board ID'
    pts_id_bits_i                            : in     std_logic_vector(31 downto 0);
-- Port for MONOSTABLE field: 'Blocking pulse enable' in reg: 'control and status register'
    pts_csr_pen_o                            : out    std_logic;
-- Port for std_logic_vector field: 'Blocking channel number' in reg: 'control and status register'
    pts_csr_chn_o                            : out    std_logic_vector(2 downto 0);
-- Port for std_logic_vector field: 'Number of pulses' in reg: 'control and status register'
    pts_csr_np_o                             : out    std_logic_vector(2 downto 0);
-- Port for BIT field: 'LED test control' in reg: 'control and status register'
    pts_csr_ldten_o                          : out    std_logic;
-- Port for BIT field: 'reset' in reg: 'control and status register'
    pts_csr_rst_o                            : out    std_logic;
-- Port for std_logic_vector field: 'switches' in reg: 'control and status register'
    pts_csr_switch_i                         : in     std_logic_vector(7 downto 0);
-- Port for std_logic_vector field: 'RTM detection lines' in reg: 'control and status register'
    pts_csr_rtm_i                            : in     std_logic_vector(5 downto 0);
-- Ports for BIT field: 'I2C Watchdog Timeout' in reg: 'control and status register'
    pts_csr_i2c_wdto_o                       : out    std_logic;
    pts_csr_i2c_wdto_i                       : in     std_logic;
    pts_csr_i2c_wdto_load_o                  : out    std_logic
  );
end pts_regs;

architecture syn of pts_regs is

signal pts_csr_pen_dly0                         : std_logic      ;
signal pts_csr_pen_int                          : std_logic      ;
signal pts_csr_chn_int                          : std_logic_vector(2 downto 0);
signal pts_csr_np_int                           : std_logic_vector(2 downto 0);
signal pts_csr_ldten_int                        : std_logic      ;
signal pts_csr_rst_int                          : std_logic      ;
signal ack_sreg                                 : std_logic_vector(9 downto 0);
signal rddata_reg                               : std_logic_vector(31 downto 0);
signal wrdata_reg                               : std_logic_vector(31 downto 0);
signal bwsel_reg                                : std_logic_vector(3 downto 0);
signal rwaddr_reg                               : std_logic_vector(0 downto 0);
signal ack_in_progress                          : std_logic      ;
signal wr_int                                   : std_logic      ;
signal rd_int                                   : std_logic      ;
signal allones                                  : std_logic_vector(31 downto 0);
signal allzeros                                 : std_logic_vector(31 downto 0);

begin
-- Some internal signals assignments. For (foreseen) compatibility with other bus standards.
  wrdata_reg <= wb_dat_i;
  bwsel_reg <= wb_sel_i;
  rd_int <= wb_cyc_i and (wb_stb_i and (not wb_we_i));
  wr_int <= wb_cyc_i and (wb_stb_i and wb_we_i);
  allones <= (others => '1');
  allzeros <= (others => '0');
-- 
-- Main register bank access process.
  process (clk_sys_i, rst_n_i)
  begin
    if (rst_n_i = '0') then 
      ack_sreg <= "0000000000";
      ack_in_progress <= '0';
      rddata_reg <= "00000000000000000000000000000000";
      pts_csr_pen_int <= '0';
      pts_csr_chn_int <= "000";
      pts_csr_np_int <= "000";
      pts_csr_ldten_int <= '0';
      pts_csr_rst_int <= '0';
      pts_csr_i2c_wdto_load_o <= '0';
    elsif rising_edge(clk_sys_i) then
-- advance the ACK generator shift register
      ack_sreg(8 downto 0) <= ack_sreg(9 downto 1);
      ack_sreg(9) <= '0';
      if (ack_in_progress = '1') then
        if (ack_sreg(0) = '1') then
          pts_csr_pen_int <= '0';
          pts_csr_i2c_wdto_load_o <= '0';
          ack_in_progress <= '0';
        else
          pts_csr_i2c_wdto_load_o <= '0';
        end if;
      else
        if ((wb_cyc_i = '1') and (wb_stb_i = '1')) then
          case rwaddr_reg(0) is
          when '0' => 
            if (wb_we_i = '1') then
            end if;
            rddata_reg(31 downto 0) <= pts_id_bits_i;
            ack_sreg(0) <= '1';
            ack_in_progress <= '1';
          when '1' => 
            if (wb_we_i = '1') then
              pts_csr_pen_int <= wrdata_reg(0);
              pts_csr_chn_int <= wrdata_reg(3 downto 1);
              pts_csr_np_int <= wrdata_reg(6 downto 4);
              pts_csr_ldten_int <= wrdata_reg(7);
              pts_csr_rst_int <= wrdata_reg(15);
              pts_csr_i2c_wdto_load_o <= '1';
            end if;
            rddata_reg(0) <= '0';
            rddata_reg(3 downto 1) <= pts_csr_chn_int;
            rddata_reg(6 downto 4) <= pts_csr_np_int;
            rddata_reg(7) <= pts_csr_ldten_int;
            rddata_reg(15) <= pts_csr_rst_int;
            rddata_reg(23 downto 16) <= pts_csr_switch_i;
            rddata_reg(29 downto 24) <= pts_csr_rtm_i;
            rddata_reg(30) <= pts_csr_i2c_wdto_i;
            rddata_reg(8) <= 'X';
            rddata_reg(9) <= 'X';
            rddata_reg(10) <= 'X';
            rddata_reg(11) <= 'X';
            rddata_reg(12) <= 'X';
            rddata_reg(13) <= 'X';
            rddata_reg(14) <= 'X';
            rddata_reg(31) <= 'X';
            ack_sreg(2) <= '1';
            ack_in_progress <= '1';
          when others =>
-- prevent the slave from hanging the bus on invalid address
            ack_in_progress <= '1';
            ack_sreg(0) <= '1';
          end case;
        end if;
      end if;
    end if;
  end process;
  
  
-- Drive the data output bus
  wb_dat_o <= rddata_reg;
-- bits
-- Blocking pulse enable
  process (clk_sys_i, rst_n_i)
  begin
    if (rst_n_i = '0') then 
      pts_csr_pen_dly0 <= '0';
      pts_csr_pen_o <= '0';
    elsif rising_edge(clk_sys_i) then
      pts_csr_pen_dly0 <= pts_csr_pen_int;
      pts_csr_pen_o <= pts_csr_pen_int and (not pts_csr_pen_dly0);
    end if;
  end process;
  
  
-- Blocking channel number
  pts_csr_chn_o <= pts_csr_chn_int;
-- Number of pulses
  pts_csr_np_o <= pts_csr_np_int;
-- LED test control
  pts_csr_ldten_o <= pts_csr_ldten_int;
-- reset
  pts_csr_rst_o <= pts_csr_rst_int;
-- switches
-- RTM detection lines
-- I2C Watchdog Timeout
  pts_csr_i2c_wdto_o <= wrdata_reg(30);
  rwaddr_reg <= wb_adr_i;
  wb_stall_o <= (not ack_sreg(0)) and (wb_stb_i and wb_cyc_i);
-- ACK signal generation. Just pass the LSB of ACK counter.
  wb_ack_o <= ack_sreg(0);
end syn;
