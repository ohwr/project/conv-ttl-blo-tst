--==============================================================================
-- CERN (BE-CO-HT)
-- PTS for the RTM of the CONV-TTL-BLO project
--==============================================================================
--
-- unit name: pts_conv_ttl_blo_rtm.vhd
--
-- author: Theodor-Adrian Stana (t.stana@cern.ch)
--
-- version: 1.0
--
-- description: Top entity of the HDL for the Production Test Suite (PTS) of the
--              CONV-TTL-BLO-RTM board.
--
-- dependencies:
--    bicolor_led_ctrl_pkg.vhd
--    genram_pkg.vhd
--    gencores_pkg.vhd
--    wishbone_pkg.vhd
--
--==============================================================================
-- GNU LESSER GENERAL PUBLIC LICENSE
--==============================================================================
-- This source file is free software; you can redistribute it and/or modify it
-- under the terms of the GNU Lesser General Public License as published by the
-- Free Software Foundation; either version 2.1 of the License, or (at your
-- option) any later version. This source is distributed in the hope that it
-- will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
-- See the GNU Lesser General Public License for more details. You should have
-- received a copy of the GNU Lesser General Public License along with this
-- source; if not, download it from http://www.gnu.org/licenses/lgpl-2.1.html
--==============================================================================
-- last changes:
--   13-01-2014   Theodor Stana       Created file
--==============================================================================
-- TODO: -
--==============================================================================
library ieee;
library unisim;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use unisim.vcomponents.all;

use work.genram_pkg.all;
use work.gencores_pkg.all;
use work.wishbone_pkg.all;

entity pts_conv_ttl_blo_rtm is
  generic
  (
    g_nr_ttl_chan : natural := 6;
    g_nr_inv_chan : natural := 4
  );
  port
  (
    -- Clocks
    -- 20 MHz from VCXO
    clk20_vcxo_i           : in  std_logic;
    -- 125 MHz from clock generator
    fpga_clk_p_i           : in  std_logic;
    fpga_clk_n_i           : in  std_logic;

    -- LEDs
    led_ctrl0_o            : out std_logic;
    led_ctrl0_oen_o        : out std_logic;
    led_ctrl1_o            : out std_logic;
    led_ctrl1_oen_o        : out std_logic;
    led_multicast_2_0_o    : out std_logic;
    led_multicast_3_1_o    : out std_logic;
    led_wr_gmt_ttl_ttln_o  : out std_logic;
    led_wr_link_syserror_o : out std_logic;
    led_wr_ok_syspw_o      : out std_logic;
    led_wr_ownaddr_i2c_o   : out std_logic;

    -- I/Os for pulses
    pulse_front_led_n_o    : out std_logic_vector(g_nr_ttl_chan downto 1);
    pulse_rear_led_n_o     : out std_logic_vector(g_nr_ttl_chan downto 1);
    fpga_input_ttl_n_i     : in  std_logic_vector(g_nr_ttl_chan downto 1);
    fpga_out_ttl_o         : out std_logic_vector(g_nr_ttl_chan downto 1);
    fpga_blo_in_i          : in  std_logic_vector(g_nr_ttl_chan downto 1);
    fpga_trig_blo_o        : out std_logic_vector(g_nr_ttl_chan downto 1);
    inv_in_n_i             : in  std_logic_vector(g_nr_inv_chan downto 1);
    inv_out_o              : out std_logic_vector(g_nr_inv_chan downto 1);

    -- Output enable lines
    fpga_oe_o              : out std_logic;
    fpga_blo_oe_o          : out std_logic;
    fpga_trig_ttl_oe_o     : out std_logic;
    fpga_inv_oe_o          : out std_logic;

    --TTL/INV_TTL_N
    ttl_switch_n_i         : in  std_logic;

    extra_switch_n_i       : in  std_logic_vector(7 downto 1);

    -- Lines for the i2c_slave
    scl_i                  : in  std_logic;
    scl_o                  : out std_logic;
    scl_oe_o               : out std_logic;
    sda_i                  : in  std_logic;
    sda_o                  : out std_logic;
    sda_oe_o               : out std_logic;
    fpga_ga_i              : in  std_logic_vector(4 downto 0);
    fpga_gap_i             : in  std_logic;

    -- Flash memory lines
    fpga_prom_cclk_o       : out std_logic;
    fpga_prom_cso_b_n_o    : out std_logic;
    fpga_prom_mosi_o       : out std_logic;
    fpga_prom_miso_i       : in  std_logic;

    -- Blocking power supply reset line
    mr_n_o                 : out std_logic;

    -- Thermometer line
    thermometer_b          : inout std_logic;

    -- PLL DACs
    -- DAC1: 20 MHz VCXO control
    fpga_plldac1_din_o     : out std_logic;
    fpga_plldac1_sclk_o    : out std_logic;
    fpga_plldac1_sync_n_o  : out std_logic;
    -- DAC2: 125 MHz clock generator control
    fpga_plldac2_din_o     : out std_logic;
    fpga_plldac2_sclk_o    : out std_logic;
    fpga_plldac2_sync_n_o  : out std_logic;

    -- SFP lines
    fpga_sfp_los_i         : in    std_logic;
    fpga_sfp_mod_def0_i    : in    std_logic;
    fpga_sfp_rate_select_o : out   std_logic;
    fpga_sfp_mod_def1_b    : inout std_logic;
    fpga_sfp_mod_def2_b    : inout std_logic;
    fpga_sfp_tx_disable_o  : out   std_logic;
    fpga_sfp_tx_fault_i    : in    std_logic;

    -- RTM identifiers, should match with the expected values
    fpga_rtmm_n_i          : in std_logic_vector(2 downto 0);
    fpga_rtmp_n_i          : in std_logic_vector(2 downto 0)
  );
end pts_conv_ttl_blo_rtm;


architecture behav of pts_conv_ttl_blo_rtm is

  --============================================================================
  -- Type declarations
  --============================================================================
  type t_pulse_counter is array (1 to g_nr_ttl_chan) of unsigned(31 downto 0);
  type t_pulse_led_cnt is array (1 to g_nr_ttl_chan) of unsigned(18 downto 0);
  type t_pulses_to_go  is array (1 to g_nr_ttl_chan) of unsigned( 2 downto 0);

  --============================================================================
  -- Constant declarations
  --============================================================================
  -- Board ID -- ASCII-encoded "TBLO" string
  constant c_board_id        : std_logic_vector(31 downto 0) := x"54424c4f";

  -- Number of Wishbone masters and slaves, for wb_crossbar
  constant c_nr_masters      : natural := 1;
  constant c_nr_slaves       : natural := 2;

  -----------------------------------------
  -- Memory map
  -- * all registers are word-addressable
  -- * all registers are word aligned
  -----------------------------------------
  --    pts_regs      [000-004]
  --    pulse_cnt     [100-12c]
  -----------------------------------------
  -- slave order definitions
  constant c_slv_pts_regs      : natural := 0;
  constant c_slv_pulse_cntrs   : natural := 1;

  -- base address definitions
  constant c_addr_pts_regs     : t_wishbone_address := x"00000000";
  constant c_addr_pulse_cntrs  : t_wishbone_address := x"00000100";

  -- address mask definitions
  constant c_mask_pts_regs     : t_wishbone_address := x"00000ff0";
  constant c_mask_pulse_cntrs  : t_wishbone_address := x"00000f00";

  -- addresses constant for Wishbone crossbar
  constant c_addresses  : t_wishbone_address_array(c_nr_slaves-1 downto 0)
                        := (
                             c_slv_pts_regs     => c_addr_pts_regs,
                             c_slv_pulse_cntrs  => c_addr_pulse_cntrs
                           );

  -- masks constant for Wishbone crossbar
  constant c_masks      : t_wishbone_address_array(c_nr_slaves-1 downto 0)
                        := (
                             c_slv_pts_regs     => c_mask_pts_regs,
                             c_slv_pulse_cntrs  => c_mask_pulse_cntrs
                           );

  -- Delay, pulse width and period constants for the pulse_gen_gp component
  constant c_del_ch_1_4  : std_logic_vector(31 downto 0) := x"00000000";
  constant c_del_ch_2_5  : std_logic_vector(31 downto 0) := x"001e8480";
  constant c_del_ch_3_6  : std_logic_vector(31 downto 0) := x"003d0900";
  constant c_pw          : std_logic_vector(31 downto 0) := x"00000018";
  constant c_per         : std_logic_vector(31 downto 0) := x"001e8480";

  --============================================================================
  -- Component declarations
  --============================================================================
  -- Reset generator component
  -- (use: global reset generation, output reset generation)
  component reset_gen is
    generic
    (
      -- Reset time in number of clk_i cycles
      g_reset_time : positive := 2_000_000
    );
    port
    (
      clk_i   : in  std_logic;
      rst_i   : in  std_logic;
      rst_n_o : out std_logic
    );
  end component reset_gen;

  -- RTM detector component
  -- (use: detect the presence of an RTM/P module)
  component rtm_detector is
    port
    (
      rtmm_i    : in std_logic_vector(2 downto 0);
      rtmp_i    : in std_logic_vector(2 downto 0);
      rtmm_ok_o : out std_logic;
      rtmp_ok_o : out std_logic
    );
  end component rtm_detector;

  -- Regs to test I2C operation
  component pts_regs is
    port (
      rst_n_i                                  : in     std_logic;
      clk_sys_i                                : in     std_logic;
      wb_adr_i                                 : in     std_logic_vector(0 downto 0);
      wb_dat_i                                 : in     std_logic_vector(31 downto 0);
      wb_dat_o                                 : out    std_logic_vector(31 downto 0);
      wb_cyc_i                                 : in     std_logic;
      wb_sel_i                                 : in     std_logic_vector(3 downto 0);
      wb_stb_i                                 : in     std_logic;
      wb_we_i                                  : in     std_logic;
      wb_ack_o                                 : out    std_logic;
      wb_stall_o                               : out    std_logic;
  -- Port for std_logic_vector field: 'bits' in reg: 'Board ID'
      pts_id_bits_i                            : in     std_logic_vector(31 downto 0);
  -- Port for BIT field: 'Blocking pulse enable' in reg: 'control and status register'
      pts_csr_pen_o                            : out    std_logic;
  -- Port for std_logic_vector field: 'Blocking channel number' in reg: 'control and status register'
      pts_csr_chn_o                            : out    std_logic_vector(2 downto 0);
  -- Port for std_logic_vector field: 'Number of pulses' in reg: 'control and status register'
      pts_csr_np_o                             : out    std_logic_vector(2 downto 0);
  -- Port for BIT field: 'LED test control' in reg: 'control and status register'
  -- Port for BIT field: 'LED test control' in reg: 'control and status register'
      pts_csr_ldten_o                          : out    std_logic;
  -- Port for BIT field: 'reset' in reg: 'control and status register'
      pts_csr_rst_o                            : out    std_logic;
  -- Port for std_logic_vector field: 'switches' in reg: 'control and status register'
      pts_csr_switch_i                         : in     std_logic_vector(7 downto 0);
  -- Port for std_logic_vector field: 'RTM detection lines' in reg: 'control and status register'
      pts_csr_rtm_i                            : in     std_logic_vector(5 downto 0);
  -- Ports for BIT field: 'I2C Watchdog Timeout' in reg: 'control and status register'
      pts_csr_i2c_wdto_o                       : out    std_logic;
      pts_csr_i2c_wdto_i                       : in     std_logic;
      pts_csr_i2c_wdto_load_o                  : out    std_logic
    );
  end component pts_regs;

  -- Fixed-frequency pulse generator component
  -- (use: generate the first pulse that gets replicated from one channel to
  -- another, in the TTL pulse test)
  component pulse_gen_gp is
    port
    (
      -- Input clock and active-low reset
      clk_i    : in  std_logic;
      rst_n_i  : in  std_logic;

      -- Active high enable signal
      en_i     : in  std_logic;

      -- Delay, pulse width and period inputs, in number of clk_i cycles
      delay_i  : in  std_logic_vector(31 downto 0);
      pwidth_i : in  std_logic_vector(31 downto 0);
      per_i    : in  std_logic_vector(31 downto 0);

      -- Output pulse signal
      pulse_o  : out std_logic
    );
  end component pulse_gen_gp;

  -- Pulse counter Wishbone regs component
  -- use: TTL, BLO tests
  component pulse_cnt_wb is
    port
    (
      rst_n_i                                  : in     std_logic;
      clk_sys_i                                : in     std_logic;
      wb_adr_i                                 : in     std_logic_vector(3 downto 0);
      wb_dat_i                                 : in     std_logic_vector(31 downto 0);
      wb_dat_o                                 : out    std_logic_vector(31 downto 0);
      wb_cyc_i                                 : in     std_logic;
      wb_sel_i                                 : in     std_logic_vector(3 downto 0);
      wb_stb_i                                 : in     std_logic;
      wb_we_i                                  : in     std_logic;
      wb_ack_o                                 : out    std_logic;
      wb_stall_o                               : out    std_logic;
  -- Port for std_logic_vector field: 'number of pulses' in reg: 'CH1 output'
      pulse_cnt_ch1o_val_i                     : in     std_logic_vector(31 downto 0);
  -- Port for std_logic_vector field: 'number of pulses' in reg: 'CH1 input'
      pulse_cnt_ch1i_val_i                     : in     std_logic_vector(31 downto 0);
  -- Port for std_logic_vector field: 'number of pulses' in reg: 'CH2 output'
      pulse_cnt_ch2o_val_i                     : in     std_logic_vector(31 downto 0);
  -- Port for std_logic_vector field: 'number of pulses' in reg: 'CH2 input'
      pulse_cnt_ch2i_val_i                     : in     std_logic_vector(31 downto 0);
  -- Port for std_logic_vector field: 'number of pulses' in reg: 'CH3 output'
      pulse_cnt_ch3o_val_i                     : in     std_logic_vector(31 downto 0);
  -- Port for std_logic_vector field: 'number of pulses' in reg: 'CH3 input'
      pulse_cnt_ch3i_val_i                     : in     std_logic_vector(31 downto 0);
  -- Port for std_logic_vector field: 'number of pulses' in reg: 'CH4 output'
      pulse_cnt_ch4o_val_i                     : in     std_logic_vector(31 downto 0);
  -- Port for std_logic_vector field: 'number of pulses' in reg: 'CH4 input'
      pulse_cnt_ch4i_val_i                     : in     std_logic_vector(31 downto 0);
  -- Port for std_logic_vector field: 'number of pulses' in reg: 'CH5 output'
      pulse_cnt_ch5o_val_i                     : in     std_logic_vector(31 downto 0);
  -- Port for std_logic_vector field: 'number of pulses' in reg: 'CH5 input'
      pulse_cnt_ch5i_val_i                     : in     std_logic_vector(31 downto 0);
  -- Port for std_logic_vector field: 'number of pulses' in reg: 'CH6 output'
      pulse_cnt_ch6o_val_i                     : in     std_logic_vector(31 downto 0);
  -- Port for std_logic_vector field: 'number of pulses' in reg: 'CH6 input'
      pulse_cnt_ch6i_val_i                     : in     std_logic_vector(31 downto 0)
    );
  end component pulse_cnt_wb;

  --============================================================================
  -- Signal declarations
  --============================================================================
  -- Global reset signals
  signal rst_n               : std_logic;

  -- Output enable signals
  signal oe, blo_oe          : std_logic;

  -- Signal for controlling the bicolor LED matrix
  signal bicolor_led_state   : std_logic_vector(23 downto 0);

  -- Wishbone crossbar signals
  signal xbar_slave_in       : t_wishbone_slave_in_array   (c_nr_masters - 1 downto 0);
  signal xbar_slave_out      : t_wishbone_slave_out_array  (c_nr_masters - 1 downto 0);
  signal xbar_master_in      : t_wishbone_master_in_array  (c_nr_slaves  - 1 downto 0);
  signal xbar_master_out     : t_wishbone_master_out_array (c_nr_slaves  - 1 downto 0);

  -- I2C bridge signals
  signal i2c_tip             : std_logic;
  signal i2c_err_p           : std_logic;
  signal i2c_wdto_p          : std_logic;
  signal i2c_addr            : std_logic_vector(6 downto 0);
  signal led_i2c_err         : std_logic;
  signal led_i2c             : std_logic;
  signal led_i2c_clkdiv      : unsigned(18 downto 0);
  signal led_i2c_cnt         : unsigned( 2 downto 0);
  signal led_i2c_blink       : std_logic;

  -- LED signals
  signal pulse_led_cnt       : t_pulse_led_cnt;
  signal pulse_led           : std_logic_vector(g_nr_ttl_chan downto 1);
  signal led_test_en         : std_logic;
  signal led_seq             : unsigned(2 downto 0);
  signal test_led            : std_logic_vector(g_nr_ttl_chan downto 1);
  signal cnt_halfsec         : unsigned(23 downto 0);

  -- General-purpose signals to/from PTS registers component
  signal rtm_lines           : std_logic_vector(5 downto 0);
  signal switches            : std_logic_vector(7 downto 0);
  signal rst_fr_reg          : std_logic;
  signal wdto_bit            : std_logic;
  signal wdto_bit_rst        : std_logic;
  signal wdto_bit_rst_ld     : std_logic;

  -- Pulse test signals
  signal pen_p               : std_logic;
  signal nr_pulses           : std_logic_vector(2 downto 0);
  signal chan_nr             : std_logic_vector(2 downto 0);

  signal pulse_en            : std_logic_vector(g_nr_ttl_chan downto 1);
  signal ptogo_cnt           : t_pulses_to_go;

  signal pin_d0              : std_logic_vector(g_nr_ttl_chan downto 1);
  signal pin_d1              : std_logic_vector(g_nr_ttl_chan downto 1);
  signal pin_d2              : std_logic_vector(g_nr_ttl_chan downto 1);
  signal pin_redge_p         : std_logic_vector(g_nr_ttl_chan downto 1);

  signal pout                : std_logic_vector(g_nr_ttl_chan downto 1);
  signal pout_d0             : std_logic_vector(g_nr_ttl_chan downto 1);
  signal pout_redge_p        : std_logic_vector(g_nr_ttl_chan downto 1);

  -- Pulse counter signals
  signal cntin, cntout       : t_pulse_counter;

  -- Other board-wide signals
  signal rtmm, rtmp          : std_logic_vector(2 downto 0);
  signal rtmm_ok, rtmp_ok    : std_logic;

begin

  --============================================================================
  -- Internal and external reset generation
  --============================================================================
  -- Configure reset generator for 96ms power-on reset
  cmp_reset_gen : reset_gen
    generic map
    (
      -- Reset time: 50ns * 2 * (10**6) = 100 ms
      g_reset_time => 2*(10**6)
    )
    port map
    (
      clk_i   => clk20_vcxo_i,
      rst_i   => rst_fr_reg,
      rst_n_o => rst_n
    );

  mr_n_o <= rst_n;

  --============================================================================
  -- I2C bridge logic
  --============================================================================
  i2c_addr <= "10" & fpga_ga_i;

  -- Instantiate I2C bridge component
  --
  -- FSM watchdog timeout timer:
  -- * consider bit period of 30 us
  -- * 10 bits / byte transfer => 300 us
  -- * 40 bytes in one transfer => 12000 us
  -- * clk_i period = 50 ns => g_fsm_wdt = 12000 us / 50 ns = 240000
  -- * multiply by two for extra safety => g_fsm_wdt = 480000
  -- * Time to watchdog timeout: 480000 * 50ns = 24 ms
  cmp_i2c_bridge : wb_i2c_bridge
    generic map
    (
      g_fsm_wdt => 480000
    )
    port map
    (
      -- Clock, reset
      clk_i      => clk20_vcxo_i,
      rst_n_i    => rst_n,

      -- I2C lines
      scl_i      => scl_i,
      scl_o      => scl_o,
      scl_en_o   => scl_oe_o,
      sda_i      => sda_i,
      sda_o      => sda_o,
      sda_en_o   => sda_oe_o,

      -- I2C address and status
      i2c_addr_i => i2c_addr,

      -- TIP and ERR outputs
      tip_o      => i2c_tip,
      err_p_o    => i2c_err_p,
      wdto_p_o   => i2c_wdto_p,

      -- Wishbone master signals
      wbm_stb_o  => xbar_slave_in(0).stb,
      wbm_cyc_o  => xbar_slave_in(0).cyc,
      wbm_sel_o  => xbar_slave_in(0).sel,
      wbm_we_o   => xbar_slave_in(0).we,
      wbm_dat_i  => xbar_slave_out(0).dat,
      wbm_dat_o  => xbar_slave_in(0).dat,
      wbm_adr_o  => xbar_slave_in(0).adr,
      wbm_ack_i  => xbar_slave_out(0).ack,
      wbm_rty_i  => xbar_slave_out(0).rty,
      wbm_err_i  => xbar_slave_out(0).err
    );

  -- Process to blink the LED when an I2C transfer is in progress
  -- blinks four times per transfer
  -- blink width : 20 ms
  -- blink period: 40 ms
  p_i2c_blink : process(clk20_vcxo_i)
  begin
    if rising_edge(clk20_vcxo_i) then
      if (rst_n = '0') then
        led_i2c_clkdiv <= (others => '0');
        led_i2c_cnt    <= (others => '0');
        led_i2c        <= '0';
        led_i2c_blink  <= '0';
      else
        case led_i2c_blink is

          when '0' =>
            led_i2c <= '0';
            if (i2c_tip = '1') then
              led_i2c_blink <= '1';
            end if;

          when '1' =>
            led_i2c_clkdiv <= led_i2c_clkdiv + 1;
            if (led_i2c_clkdiv = 399999) then
              led_i2c_clkdiv <= (others => '0');
              led_i2c_cnt    <= led_i2c_cnt + 1;
              led_i2c        <= not led_i2c;
              if (led_i2c_cnt = 7) then
                led_i2c_cnt <= (others => '0');
                led_i2c_blink <= '0';
              end if;
            end if;

          when others =>
            led_i2c_blink <= '0';

        end case;
      end if;
    end if;
  end process p_i2c_blink;

  -- Process to set the I2C error LED signal for display on the front panel
  -- of the front module. The I2C error LED signal is permanently set once an
  -- error is detected from the bridge module.
  p_i2c_err_led : process (clk20_vcxo_i) is
  begin
    if rising_edge(clk20_vcxo_i) then
      if (rst_n = '0') then
        led_i2c_err <= '0';
      elsif (i2c_err_p = '1') then
        led_i2c_err <= '1';
      end if;
    end if;
  end process p_i2c_err_led;

  -- Register for the WDTO bit in the SR, cleared by writing a '1'
  p_sr_wdto_bit : process (clk20_vcxo_i)
  begin
    if rising_edge(clk20_vcxo_i) then
      if (rst_n = '0') then
        wdto_bit <= '0';
      elsif (i2c_wdto_p = '1') then
        wdto_bit <= '1';
      elsif (wdto_bit_rst_ld = '1') and (wdto_bit_rst = '1') then
        wdto_bit <= '0';
      end if;
    end if;
  end process p_sr_wdto_bit;

  --============================================================================
  -- Instantiation and connection of the main Wishbone crossbar
  --============================================================================
  xbar_master_in(0).int <= '0';
  xbar_master_in(0).err <= '0';

  cmp_wb_crossbar : xwb_crossbar
  generic map
  (
    g_num_masters => c_nr_masters,
    g_num_slaves  => c_nr_slaves,
    g_registered  => false,
    g_address     => c_addresses,
    g_mask        => c_masks
  )
  port map
  (
    clk_sys_i => clk20_vcxo_i,
    rst_n_i   => rst_n,
    slave_i   => xbar_slave_in,
    slave_o   => xbar_slave_out,
    master_i  => xbar_master_in,
    master_o  => xbar_master_out
  );

  --============================================================================
  -- PTS registers
  --============================================================================
  -- Assign the RTM detection lines and switches signals
  rtm_lines <= fpga_rtmp_n_i & fpga_rtmm_n_i;
  switches  <= ttl_switch_n_i & extra_switch_n_i;

  -- Regs to test I2C operation
  cmp_pts_regs : pts_regs
    port map
    (
      rst_n_i                 => rst_n,
      clk_sys_i               => clk20_vcxo_i,

      wb_adr_i                => xbar_master_out(c_slv_pts_regs).adr(2 downto 2),
      wb_dat_i                => xbar_master_out(c_slv_pts_regs).dat,
      wb_dat_o                => xbar_master_in(c_slv_pts_regs).dat,
      wb_cyc_i                => xbar_master_out(c_slv_pts_regs).cyc,
      wb_sel_i                => xbar_master_out(c_slv_pts_regs).sel,
      wb_stb_i                => xbar_master_out(c_slv_pts_regs).stb,
      wb_we_i                 => xbar_master_out(c_slv_pts_regs).we,
      wb_ack_o                => xbar_master_in(c_slv_pts_regs).ack,
      wb_stall_o              => xbar_master_in(c_slv_pts_regs).stall,

      pts_id_bits_i           => c_board_id,
      pts_csr_pen_o           => pen_p,
      pts_csr_chn_o           => chan_nr,
      pts_csr_np_o            => nr_pulses,
      pts_csr_ldten_o         => led_test_en,
      pts_csr_rst_o           => rst_fr_reg,
      pts_csr_switch_i        => switches,
      pts_csr_rtm_i           => rtm_lines,
      pts_csr_i2c_wdto_o      => wdto_bit_rst,
      pts_csr_i2c_wdto_i      => wdto_bit,
      pts_csr_i2c_wdto_load_o => wdto_bit_rst_ld
    );

  --============================================================================
  -- Pulse output enable logic
  --============================================================================
  -- The general output enable is set first and the blocking, TTL
  -- and INV output enable signals are set one clock cycle later.
  p_oe : process(clk20_vcxo_i)
  begin
    if rising_edge(clk20_vcxo_i) then
      if (rst_n = '0') then
        oe     <= '0';
        blo_oe <= '0';
      else
        oe <= '1';
        if (oe = '1') then
          blo_oe <= oe;
        end if;
      end if;
    end if;
  end process p_oe;

  -- Assign output enable registers to chip outputs
  fpga_oe_o          <= oe;
  fpga_blo_oe_o      <= blo_oe;

  --============================================================================
  -- Blocking pulse test logic
  --============================================================================
  gen_pulse_logic : for i in 1 to g_nr_ttl_chan generate
    ----------------------------------------------------------------------------
    -- Pulse generation logic
    ----------------------------------------------------------------------------
    -- First, instantiate the pulse generator components
    cmp_pulse_gen : pulse_gen_gp
      port map
      (
        -- Input clock and active-low reset
        clk_i    => clk20_vcxo_i,
        rst_n_i  => rst_n,

        -- Active high enable signal
        en_i     => pulse_en(i),

        -- Delay, pulse width and period inputs, in number of clk_i cycles
        delay_i  => (others => '0'),
        pwidth_i => c_pw,
        per_i    => c_per,

        -- Output pulse signal
        pulse_o  => pout(i)
      );

    -- Then, a short pulse counter "FSM" to control generating the number of
    -- pulses desired by the user
    p_output_pulses : process (clk20_vcxo_i)
    begin
      if rising_edge(clk20_vcxo_i) then
        if (rst_n = '0') then
          pulse_en(i)  <= '0';
          ptogo_cnt(i) <= (others => '0');
        else
          case pulse_en(i) is
            when '0' =>
              -- when the user enables pulse generation on the appropriate channel,
              -- store NP from the CSR to the togo counter
              if (pen_p = '1') and (chan_nr = std_logic_vector(to_unsigned(i,3))) then
                pulse_en(i)  <= '1';
                ptogo_cnt(i) <= unsigned(nr_pulses);
              end if;
            when '1' =>
              -- decrement counter on pulse output
              if (pout_redge_p(i) = '1') then
                ptogo_cnt(i) <= ptogo_cnt(i) - 1;
              end if;

              -- disable pulse generation after generating NP pulses
              if (ptogo_cnt(i) = 0) then
                pulse_en(i) <= '0';
              end if;
            when others =>
              pulse_en(i) <= '0';
          end case;
        end if;
      end if;
    end process p_output_pulses;

    -- And finally, assign the outputs to the pulses
    fpga_trig_blo_o(i) <= pout(i);

    ----------------------------------------------------------------------------
    -- Pulse counting logic
    ----------------------------------------------------------------------------
    -- First, the process to generate sync FFs on the input pulses and the rising
    -- edge detectors on both input and output pulses
    p_redge : process (clk20_vcxo_i)
    begin
      if rising_edge(clk20_vcxo_i) then
        if (rst_n = '0') then
          pout_d0(i)      <= '0';
          pout_redge_p(i) <= '0';
          pin_d0(i)       <= '0';
          pin_d1(i)       <= '0';
          pin_d2(i)       <= '0';
          pin_redge_p(i)  <= '0';
        else
          pout_d0(i)      <= pout(i);
          pout_redge_p(i) <= pout(i) and (not pout_d0(i));
          pin_d0(i)       <= fpga_blo_in_i(i);
          pin_d1(i)       <= pin_d0(i);
          pin_d2(i)       <= pin_d1(i);
          pin_redge_p(i)  <= pin_d1(i) and (not pin_d2(i));
        end if;
      end if;
    end process p_redge;

    -- Now, the processes for the input and output pulse counters
    p_cntin : process (clk20_vcxo_i)
    begin
      if rising_edge(clk20_vcxo_i) then
        if (rst_n = '0') then
          cntin(i) <= (others => '0');
        elsif (pin_redge_p(i) = '1') then
          cntin(i) <= cntin(i) + 1;
        end if;
      end if;
    end process p_cntin;

    p_cntout : process (clk20_vcxo_i)
    begin
      if rising_edge(clk20_vcxo_i) then
        if (rst_n = '0') then
          cntout(i) <= (others => '0');
        elsif (pout_redge_p(i) = '1') then
          cntout(i) <= cntout(i) + 1;
        end if;
      end if;
    end process p_cntout;

    ----------------------------------------------------------------------------
    -- Logic to light the pulse LED
    ----------------------------------------------------------------------------
    -- LED flash length: 26 ms
    p_pulse_led : process (clk20_vcxo_i) is
    begin
      if rising_edge(clk20_vcxo_i) then
        if (rst_n = '0') then
          pulse_led_cnt(i) <= (others => '0');
          pulse_led(i)     <= '0';
        else
          case pulse_led(i) is
            when '0' =>
              if (pout_redge_p(i) = '1') then
                pulse_led(i) <= '1';
              end if;
            when '1' =>
              pulse_led_cnt(i) <= pulse_led_cnt(i) + 1;
              if (pulse_led_cnt(i) = (pulse_led_cnt(i)'range => '1')) then
                pulse_led(i) <= '0';
              end if;
            when others =>
              pulse_led(i) <= '0';
          end case;
        end if;
      end if;
    end process p_pulse_led;

  end generate gen_pulse_logic;

  --============================================================================
  -- Pulse counter registers, holding values for pulse counters of both blocking
  -- and TTL pulse repetition tests.
  --============================================================================
  cmp_pulse_cnt_wb : pulse_cnt_wb
    port map
    (
      rst_n_i              => rst_n,
      clk_sys_i            => clk20_vcxo_i,
      wb_adr_i             => xbar_master_out(c_slv_pulse_cntrs).adr(5 downto 2),
      wb_dat_i             => xbar_master_out(c_slv_pulse_cntrs).dat,
      wb_dat_o             => xbar_master_in(c_slv_pulse_cntrs).dat,
      wb_cyc_i             => xbar_master_out(c_slv_pulse_cntrs).cyc,
      wb_sel_i             => xbar_master_out(c_slv_pulse_cntrs).sel,
      wb_stb_i             => xbar_master_out(c_slv_pulse_cntrs).stb,
      wb_we_i              => xbar_master_out(c_slv_pulse_cntrs).we,
      wb_ack_o             => xbar_master_in(c_slv_pulse_cntrs).ack,
      wb_stall_o           => xbar_master_in(c_slv_pulse_cntrs).stall,

      pulse_cnt_ch1o_val_i => std_logic_vector(cntout(1)),
      pulse_cnt_ch1i_val_i => std_logic_vector(cntin(1)),
      pulse_cnt_ch2o_val_i => std_logic_vector(cntout(2)),
      pulse_cnt_ch2i_val_i => std_logic_vector(cntin(2)),
      pulse_cnt_ch3o_val_i => std_logic_vector(cntout(3)),
      pulse_cnt_ch3i_val_i => std_logic_vector(cntin(3)),
      pulse_cnt_ch4o_val_i => std_logic_vector(cntout(4)),
      pulse_cnt_ch4i_val_i => std_logic_vector(cntin(4)),
      pulse_cnt_ch5o_val_i => std_logic_vector(cntout(5)),
      pulse_cnt_ch5i_val_i => std_logic_vector(cntin(5)),
      pulse_cnt_ch6o_val_i => std_logic_vector(cntout(6)),
      pulse_cnt_ch6i_val_i => std_logic_vector(cntin(6))
    );

  --============================================================================
  -- LED test logic
  -- * test pulse LEDs
  --============================================================================
  -- The pulse status LEDs are sequenced during the LED test and are briefly lit
  -- during the pulse test; here we implement the multiplexer which selects the
  -- part of the logic which controls the pulse LEDs
  gen_pulse_leds : for i in 1 to g_nr_ttl_chan generate
    pulse_front_led_n_o(i) <= test_led(i) when (led_test_en = '1') else
                              (not pulse_led(i)) when (pulse_en(i) = '1') else
                              '1';
    pulse_rear_led_n_o(i)  <= test_led(i) when (led_test_en = '1') else
                              (not pulse_led(i)) when (pulse_en(i) = '1') else
                              '1';
  end generate gen_pulse_leds;

  -- Process to control the LED sequence counter
  p_led_seq : process (clk20_vcxo_i) is
  begin
    if rising_edge(clk20_vcxo_i) then
      if (rst_n = '0') then
        cnt_halfsec <= (others => '0');
        led_seq     <= (others => '0');
      elsif (led_test_en = '1') then
        cnt_halfsec <= cnt_halfsec + 1;
        if (cnt_halfsec = 9999999) then
          cnt_halfsec <= (others => '0');
          led_seq     <= led_seq + 1;
          if (led_seq = 6) then
            led_seq   <= (others => '0');
          end if;
        end if;
      end if;
    end if;
  end process p_led_seq;

  -- Sequence the front-panel LEDs based on the sequence counter
  test_led <= "111110" when (led_test_en = '1') and (led_seq =  1) else
              "111101" when (led_test_en = '1') and (led_seq =  2) else
              "111011" when (led_test_en = '1') and (led_seq =  3) else
              "110111" when (led_test_en = '1') and (led_seq =  4) else
              "101111" when (led_test_en = '1') and (led_seq =  5) else
              "011111" when (led_test_en = '1') and (led_seq =  6) else
              "111111";

  --============================================================================
  -- Bicolor LED matrix logic
  --============================================================================
  -- Bicolor LED controls, corresponding to the column orders on the
  -- bicolor_led_ctrl unit.

  -- WR address
  bicolor_led_state( 1 downto  0) <= c_LED_OFF;

  -- WR GMT
  bicolor_led_state( 3 downto  2) <= c_LED_OFF;

  -- WR link
  bicolor_led_state( 5 downto  4) <= c_LED_OFF;

  -- WR OK
  bicolor_led_state( 7 downto  6) <= c_LED_OFF;

  -- MULTICAST 0
  bicolor_led_state( 9 downto  8) <= c_LED_OFF;

  -- MULTICAST 1
  bicolor_led_state(11 downto 10) <= c_LED_OFF;


  -- I2C
  bicolor_led_state(13 downto 12) <= c_LED_GREEN when (led_i2c = '1') else
                                     c_LED_RED   when (led_i2c_err = '1') else
                                     c_LED_OFF;

  -- State of TTL/TTL_N switch
  bicolor_led_state(15 downto 14) <= c_LED_GREEN when (ttl_switch_n_i = '0') else
                                     c_LED_OFF;

  -- System error
  bicolor_led_state(17 downto 16) <= c_LED_RED when (rtmm_ok = '0') and (rtmp_ok = '0') else
                                     c_LED_OFF;

  -- System power
  bicolor_led_state(19 downto 18) <= c_LED_GREEN;

  -- MULTICAST 2
  bicolor_led_state(21 downto 20) <= c_LED_OFF;

  -- MULTICAST 3
  bicolor_led_state(23 downto 22) <= c_LED_OFF;

  cmp_bicolor_led_ctrl : gc_bicolor_led_ctrl
    generic map
    (
      g_NB_COLUMN    => 6,
      g_NB_LINE      => 2,
      g_clk_freq     => 20000000,
      g_refresh_rate => 250
    )
    port map
    (
      clk_i           => clk20_vcxo_i,
      rst_n_i         => rst_n,
      led_intensity_i => "1111111",
      led_state_i     => bicolor_led_state,
      column_o(0)     => led_wr_ownaddr_i2c_o,
      column_o(1)     => led_wr_gmt_ttl_ttln_o,
      column_o(2)     => led_wr_link_syserror_o,
      column_o(3)     => led_wr_ok_syspw_o,
      column_o(4)     => led_multicast_2_0_o,
      column_o(5)     => led_multicast_3_1_o,
      line_o(0)       => led_ctrl0_o,
      line_o(1)       => led_ctrl1_o,
      line_oen_o(0)   => led_ctrl0_oen_o,
      line_oen_o(1)   => led_ctrl1_oen_o
    );

  --============================================================================
  -- RTM detection logic
  --============================================================================
  rtmm <= not fpga_rtmm_n_i;
  rtmp <= not fpga_rtmp_n_i;

  cmp_rtm_detector : rtm_detector
    port map
    (
      rtmm_i    => rtmm,
      rtmp_i    => rtmp,
      rtmm_ok_o => rtmm_ok,
      rtmp_ok_o => rtmp_ok
    );

  --============================================================================
  -- Drive unused outputs with safe values
  --============================================================================
  -- Front pulse and pulse enable outputs
  fpga_trig_ttl_oe_o <= '0';
  fpga_inv_oe_o      <= '0';
  fpga_out_ttl_o     <= (others => '0');
  inv_out_o          <= (others => '0');

  -- Theremometer output to high-impedance
  thermometer_b <= 'Z';

  -- DAC outputs: enables to '1' (disable DAC comm interface) and SCK, DIN to '0'
  fpga_plldac1_sync_n_o <= '1';
  fpga_plldac1_din_o    <= '0';
  fpga_plldac1_sclk_o   <= '0';
  fpga_plldac2_sync_n_o <= '1';
  fpga_plldac2_din_o    <= '0';
  fpga_plldac2_sclk_o   <= '0';

  -- SFP lines all open-drain, set to high-impedance
  fpga_sfp_rate_select_o <= 'Z';
  fpga_sfp_mod_def1_b    <= 'Z';
  fpga_sfp_mod_def2_b    <= 'Z';
  fpga_sfp_tx_disable_o  <= 'Z';

  -- Flash lines
  fpga_prom_cclk_o       <= '0';
  fpga_prom_cso_b_n_o    <= '1';
  fpga_prom_mosi_o       <= '0';

end behav;
