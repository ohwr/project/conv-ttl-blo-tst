%==============================================================================
% Document header
%==============================================================================
\documentclass[a4paper,11pt]{article}

% Color package
\usepackage[usenames,dvipsnames]{color}

% Appendix package
\usepackage[toc,page]{appendix}

% Hyperrefs
\usepackage[
  colorlinks = true,
  linkcolor  = black,
  citecolor  = black,
  urlcolor   = blue,
]{hyperref}

\usepackage{graphicx}
\usepackage{multirow}

% Header and footer customization
\usepackage{fancyhdr}
\setlength{\headheight}{15.2pt}
\pagestyle{fancy}
\fancyhead[L]{\nouppercase{\leftmark}}
\fancyhead[R]{} 
\renewcommand{\footrulewidth}{0.4pt}

%==============================================================================
% Start of document
%==============================================================================
\begin{document}

%------------------------------------------------------------------------------
% Title
%------------------------------------------------------------------------------
\include{cern-title}

%------------------------------------------------------------------------------
% Licensing info
%------------------------------------------------------------------------------
\pagebreak

\thispagestyle{empty}

\addcontentsline{toc}{section}{Licensing information}
\section*{Licensing information}

\noindent
This document is licensed under a Creative Commons Attribution-ShareAlike 4.0
International License. If you have not received a copy of the license along with this
work, see \\
\url{http://creativecommons.org/licenses/by-sa/4.0/}

%------------------------------------------------------------------------------
% Revision history
%------------------------------------------------------------------------------
\section*{Revision history}
\addcontentsline{toc}{section}{Revision history}

\centerline
{
  \begin{tabular}{l c p{.6\textwidth}}
  \hline
  \multicolumn{1}{c}{\textbf{Date}} & \multicolumn{1}{c}{\textbf{Version}} & \multicolumn{1}{c}{\textbf{Change}} \\
  \hline
  24-01-2014 & 0.1 & First draft \\
  13-11-2014 & 0.2 & Second draft, adding licensing information, updating links in bibliography and changing some text
                     in the introduction, since the repository is moved under the CONV-TTL-BLO OHWR project \\
  \hline
  \end{tabular}
}

%------------------------------------------------------------------------------
% Table of contents, list of figs, tables
%------------------------------------------------------------------------------
\pagebreak
\pdfbookmark[1]{\contentsname}{toc}
\tableofcontents

\listoffigures
\listoftables

%------------------------------------------------------------------------------
% List of abbreviations
%------------------------------------------------------------------------------
\pagebreak
\section*{List of Abbreviations}
\begin{tabular}{l l}
HDL & Hardware Description Language \\
I$^2$C & Inter-Integrated Circuit (bus) \\
OHWR & Open-Hardware Repository \\
PTS & Production Test Suite \\
RTM & Rear-Transition Module \\
VME & VERSAmodule Eurocard \\
\end{tabular}
\addcontentsline{toc}{section}{List of abbreviations}

%==============================================================================
% SEC: Intro
%==============================================================================
\pagebreak
\section{Introduction}
\label{sec:intro}
Production Test Suite (PTS) is an environment designed for the functionality tests of
boards at the manufacturing site, right after production. It assures that boards comply
with a minimum set of quality rules in terms of soldering, mounting and PCB fabrication
process.

This document presents a high-level view of the firmware implemented on the FPGA for the 
PTS project for the CONV-TTL-RTM-BLO board. More details can be found by consulting
the HDL of the PTS, which can be obtained by cloning the \textit{conv-ttl-blo-tst} OHWR
git repository for the CONV-TTL-BLO testing project~\cite{pts-proj}.

%------------------------------------------------------------------------------
% SUBSEC: Additional docs
%------------------------------------------------------------------------------
\subsection*{Additional documentation}

\begin{itemize}
  \item CONV-TTL-BLO User Guide \cite{ctb-ug}
  \item CONV-TTL-BLO Hardware Guide \cite{ctb-hwguide}
  \item CONV-TTL-RTM-BLO PTS User Guide \cite{pts-userguide}
  \item CONV-TTL-RTM-BLO PTS Hardware Guide \cite{pts-hwguide}
\end{itemize}

%==============================================================================
% SEC: PTS system
%==============================================================================
\section{PTS system}
\label{sec:pts-sys}

Since most of the hardware to be tested on the blocking pulse repetition is on the
CONV-TTL-BLO side, only the connectors and LEDs need to be electrically tested on the
RTM board. The PTS system (Figure~\ref{fig:pts-sys}) used to test these components consists
of the test computer and the ELMA crate.

\begin{figure}[h]
  \centerline{\includegraphics[width=.9\textwidth]{fig/pts-sys}}
  \caption{PTS System}
  \label{fig:pts-sys}
\end{figure}

The ELMA crate within the PTS contains a system monitor (SysMon) board that monitors
voltage levels, temperatures and controls fan speeds for the crate fans. The crate
can be accessed via Telnet to send commands to the VME boards inside the crate.
The ELMA SysMon translates these commands into I$^2$C accesses to registers on the
VME board. It is in this manner that the CONV-TTL-BLO can be controlled remotely
in an ELMA crate.

Software on the PTS computer connects to the crate via Telnet and communicates to
the CONV-TTL-BLO, which contains logic that drives the pulse and LED lines on the
CONV-TTL-RTM-BLO. Using some PTS-specific boards which loop back the blocking signals,
pulses sent on the CONV-TTL-BLO outputs are read back on specific inputs. Pulse counters
inside the logic count the number of sent and received pulses on each channel, and the
software on the PTS computer calculates that the appropriate number of pulses have
been sent and received on a channel, informing the user.

%==============================================================================
% SEC: HDL Overview
%==============================================================================
\section{HDL overview}
\label{sec:overview}

Figure~\ref{fig:hdl-bd} represents the block diagram of the PTS firmware.

\begin{figure}[h]
  \centerline{\includegraphics[width=.8\textwidth]{fig/hdl-bd}}
  \label{fig:hdl-bd}
  \caption{Block diagram of the PTS firmware}
\end{figure}

First, I$^2$C accesses from the SysMon are translated by the \textit{wb\_i2c\_bridge}
component into Wishbone accesses to read board status data and control pulse generation
via the \textit{pts\_regs} component, or read the number of pulses sent and received by
the board via the \textit{pulse\_cnt\_wb} component. For a detailed view at the memory
map and the fields present in each register, consult the memory map in
Appendix~\ref{app:memmap}.

%--------------------------------------------------------------------------------------
% SUBSEC: Channel logic
%--------------------------------------------------------------------------------------
\subsection{Channel logic}
\label{sec:overview-chan}

On the channel logic side, which is multiplied times the number of channels on the
board under test, a VHDL process (\textit{p\_output\_pulses}) is used to control the
generation of pulses. In the PTS CSR (see Appendix~\ref{app:memmap-csr}), the PTS
software sets the number of the channel to send pulses on in the CHN field, and the number
of pulses to send in the NP field. When the PTS software also sets the PEN bit, a pulse
counter internal to the process stores the value of NP, and pulse generation is enabled
via a simple pulse generator component. The pulse counter internal to
\textit{p\_output\_pulses} decrements on each rising edge of a pulse, until it reaches
zero, at which time pulse generation is stopped.

The \textit{pulse\_gen\_gp} component simply generates active-high pulses with
a certain width, frequency and delay as set by its inputs. The width is set
via the \textit{c\_pw} VHDL constant to be 1.2~$\mu$s and the frequency is
set via the \textit{c\_freq} constant to 10 Hz.

Apart from decrementing the internal counter of the \textit{p\_output\_pulses}
process, a rising edge on the output of the \textit{pulse\_gen\_gp} component
also increments the output pulse counter on the channel. Similarly, a rising
edge on the channel pulse input line increments (after resynchronization in
the FPGA clock domain) the input pulse counter on the channel. Both the input
and output pulse counter values on each channel are stored in registers accessible
via the I$^2$C interface within the \textit{pulse\_cnt\_wb} component. The
memory map of these registers can be found in Appendix~\ref{app:memmap-pulse-cnt}.

%--------------------------------------------------------------------------------------
% SUBSEC: Pulse LED logic
%--------------------------------------------------------------------------------------
\subsection{Pulse test logic}
\label{sec:overview-leds}

In order to sequence the LEDs on the rear panel, every half a second, a counter is
incremented up to six when the LDTEN bit in the CSR is set. Based on the value of this
counter, one of the rear panel LEDs is lit.

When LDTEN is zero, the rear-panel pulse LEDs are lit under the control of the channel logic
for 26~ms every time a pulse is generated on a channel.

%======================================================================================
% SEC: Test logic
%======================================================================================
\section{Test logic}
\label{sec:test-logic}

This section summarizes the logic used in each of the tests. Detailed information about
the implementation can be found by consulting the code.

%--------------------------------------------------------------------------------------
% SUBSEC: Test 01
%--------------------------------------------------------------------------------------
\subsection{Test 01}

The PTS software reads the RTM field in the PTS CSR (see Appendix~\ref{app:memmap-csr})
and checks its value versus known existing RTM boards. An up-to-date version of existing
RTM boards for the CONV-TTL-BLO can be found on the RTM detection webpage on
OHWR~\cite{rtmdet-ohwr}.

%--------------------------------------------------------------------------------------
% SUBSEC: Test 02
%--------------------------------------------------------------------------------------
\subsection{Test 02}

The PTS software executes a sequence of outputting seven pulses on each output
channel and checks that the appropriate number of pulses is received on each channel.
The output channels are looped back to input channels via the RTM board testers~\cite{pts-hwguide} as shown
in Table~\ref{tbl:loopbacks}.

\begin{table}[h]
  \label{tbl:loopbacks}
  \caption{Output-to-input connections on Test 02}
  \centerline
  {
    \begin{tabular}{c c c c}
    \hline
    \textbf{Output} & \multicolumn{3}{c}{\textbf{Input}} \\
    \hline
    O1 & I1 & I2 & I3 \\
    O2 & I2 & I3 & I1 \\
    O3 & I3 & I1 & I2 \\
    O4 & I4 & I5 & I6 \\
    O5 & I5 & I6 & I4 \\
    O6 & I6 & I5 & I4 \\
    \hline
    \end{tabular}
  }
\end{table}

The sequence the PTS software should execute is the following:

\begin{enumerate}

  \item set CSR.CHN to 1
  \item set CSR.NP to 7
  \item set CSR.PEN high to start pulse generation; the PEN bit will reset
  automatically
  \item seven pulses are generated on channel one once every 100~ms
  \item after 700~ms, check that the input counter for channels 1, 2 and 3 is
  equal to 7
  \item set CSR.CHN to 2
  \item set CSR.NP to 7
  \item set CSR.PEN high to start pulse generation; the PEN bit will reset
  automatically
  \item seven pulses are generated on channel two once every 100~ms
  \item after 700~ms, check that the input counter for channels 1, 2 and 3 is
  equal to 14
  \item set CSR.CHN to 3
  \item set CSR.NP to 7
  \item set CSR.PEN high to start pulse generation; the PEN bit will reset
  automatically
  \item seven pulses are generated on channel three once every 100~ms
  \item after 700~ms, check that the input counter for channels 1, 2 and 3 is
  equal to 21
  \item repeat steps 1-15 with channels 4, 5 and 6
\end{enumerate}

%--------------------------------------------------------------------------------------
% SUBSEC: Test 03
%--------------------------------------------------------------------------------------
\subsection{Test 03}

The PTS software sets the LDTEN bit high to initiate pulse LED sequencing. The
pulse LEDs are sequenced in turn by the logic as specified in Section~\ref{sec:overview-leds}
and the user is asked to confirm all of them light up.

%======================================================================================
% SEC: Folder structure
%======================================================================================
\section{Folder structure}
\label{sec:fold-struct}

The folder structure for the project is presented below.

\renewcommand{\labelitemi}{$\rightarrow$}
\renewcommand{\labelitemii}{$\rightarrow$}
\renewcommand{\labelitemiii}{$\rightarrow$}
\renewcommand{\labelitemiv}{$\rightarrow$}

\begin{itemize}
  \item conv-ttl-blo-tst/rtm/hdl
  \begin{itemize}
    \item ip\_cores/
    \begin{itemize}
      \item general-cores/
    \end{itemize}
    \item modules/
    \begin{itemize}
      \item bicolor\_led\_ctrl/
      \item pulse\_cnt\_wb.wb
      \item pulse\_cnt\_wb.vhd
      \item pulse\_gen\_gp.vhd
      \item pts\_regs.vhd
      \item pts\_regs.wb
      \item reset\_gen.vhd
      \item rtm\_detector.vhd
    \end{itemize}
    \item syn/
    \item top/
    \begin{itemize}
      \item pts\_conv\_ttl\_blo\_rtm.vhd
      \item pts\_conv\_ttl\_blo\_rtm.ucf
    \end{itemize}
  \end{itemize}
\end{itemize}

HDL files are categorized within this PTS as modules and the top-level file. The top-level
HDL file can be found in the \textit{top/} folder along with the .ucf constraints file. Modules
instantiated by the top-level file can be found in the \textit{modules/} folder for PTS-specific
modules, and in the \textit{ip\_cores/} folder for IP cores imported from outside the PTS
environment. The \textit{syn/} folder contains synthesis-specific files, such as the ISE project
file, as well as output files after synthesis.

%======================================================================================
% SEC: Getting around the code
%======================================================================================
\section{Getting around the code}
\label{sec:fold-struct}

Code in the top-level files is organized in code sections. A code section is a piece of code
pertaining to a certain part of the design, where component instantiations and input and
output port assignments are made. For example, there is a section pertaining to
pulse repetition, where there is a generate block to generate the logic necessary for pulse
repetition on each channel, including the pulse status LEDs.

\begin{figure}[h]
  \centerline{\includegraphics[width=.75\textwidth]{fig/arch}}
  \caption{VHDL architecture of the PTS firmware}
  \label{fig:arch}
\end{figure}

Ports and signals usually follow the coding guideline on OHWR~\cite{coding-guidelines}. Most of the
top-level ports of the firmware are lower-case versions of their schematics counterparts. The
exceptions from this are due to either net names that could not be syntactically represented in
VHDL, or net names that have been made clearer in VHDL code.

The declarative part of the architecture is organized as shown in Figure~\ref{fig:arch}~(a).
Types are declared right after the architecture declaration, followed by constant
declarations, followed by component declarations, after which the various signals
are declared.

The body of the architecture is organised in code sections as shown in
Figure~\ref{fig:arch}~(b). It starts with the instantiation of the \textit{reset\_gen}
component which generates the board-wide reset. Then, the \textit{wb\_i2c\_bridge}
component is instantiated and the communication watchdog register is defined. This
register is defined at the top-level since it is defined as a \textit{load\_ext}
via \textit{wbgen2}. Logic is also defined here for blinking in green the I2C bicolor
LED on the CONV-TTL-BLO front panel when an I$^2$C transfer takes places and for
lighting the same LED red on an I$^2$C error.

The I$^2$C bridge section is followed by the short section where the Wishbone
crossbar is instantiated. After this, the PTS general-purpose registers are instantiated,
followed by the pulse channel logic. Since this logic is the same for all six
channels, a generate statement is used to generate the logic on each channel.

After the pulse channel logic, the \textit{pulse\_cnt\_wb} component is instantiated
and connected to the channel pulse counters. Then, the next section defines the logic 
for the counter incrementing every half second to control which rear panel pulse
LED is lit and sets the pulse LED outputs based on the state of the LDTEN bit in
the CSR.

The code ends with two small sections, one which connects the \textit{bicolor\_led\_ctrl}
component to light the bicolor LEDs on the CONV-TTL-BLO, and one which instantiates
the simple \textit{rtm\_detector} component, which is used for lighting one of the
bicolor LEDs.

%==============================================================================
% Appendices
%==============================================================================
\pagebreak
\begin{appendices}

%==============================================================================
% APP: Memmap
%==============================================================================
\section{Memory map}
\label{app:memmap}

Table~\ref{tbl:memmap} shows the complete memory map of the firmware. The
following sections list the memory map of each peripheral.

\begin{table}[h]
\caption{CONV-TTL-BLO memory map}
\label{tbl:memmap}
\centerline
{
  \begin{tabular}{l l l p{.4\textwidth}}
  \hline
  \multicolumn{1}{c}{\textbf{Periph.}} & \multicolumn{2}{c}{\textbf{Address}} & \multicolumn{1}{c}{\textbf{Description}} \\
  & \multicolumn{1}{c}{\textbf{Base}} & \multicolumn{1}{c}{\textbf{End}} & \\
  \hline
  \textit{pts\_regs}      & 0x000 & 0x004 & Control and status register \\
  \textit{pulse\_cnt\_wb} & 0x100 & 0x12c & MultiBoot module \\
  \hline
  \end{tabular}
}
\end{table}

%------------------------------------------------------------------------------
% SUBSEC: CSR
%------------------------------------------------------------------------------
\subsection{Control and status registers}
\label{app:memmap-csr}

Base address: 0x000

\begin{table}[h]
    \begin{tabular}{l l p{.6\textwidth}}
    \textbf{Offset} & \textbf{Name} & \textbf{Description} \\
    0x0 & BIDR & Board ID Register \\
    0x4 & CSR  & Control and Status Register \\
    \end{tabular}
\end{table}

%------------------------------------------------------------------------------
\subsubsection{Board ID Register}

\begin{tabular}{l l c c l}
\textbf{Bits} & \textbf{Field} & \textbf{Access} & \textbf{Default} & \textbf{Description} \\
31..0         & ID             & R/O             & 0x54424c4f       & Board ID \\
\end{tabular}

\vspace*{11pt}

\noindent
{
  \begin{tabular}{l l}
  \textbf{Field} & \textbf{Description} \\
  ID             & Board ID (ASCII string \textbf{TBLO}) \\
  \end{tabular}
}

%------------------------------------------------------------------------------
\subsubsection{Control and Status Register}

\begin{tabular}{l l c c p{.35\textwidth}}
\textbf{Bits} & \textbf{Field}    & \textbf{Access} & \textbf{Default} & \textbf{Description} \\
0             & PEN               & W/O             & 0                & Enable pulse output \\
3..1          & CHN               & R/W             & 0                & Channel number to send pulses on \\
6..4          & NP                & R/W             & 0                & Number of pulses to send on the channel \\
7             & LDTEN             & R/W             & 0                & Enable LED test \\
14..8         & \textit{Reserved} & --              & X                & \\
15            & RST               & R/W             & 0                & Reset logic \\
23..16        & SWITCHES          & R/O             & X                & Switch status \\
29..24        & RTM               & R/O             & X                & RTM detection lines \\
30            & CWDTO             & R/W             & 0                & Communication watchdog timeout \\
31            & \textit{Reserved} & --              & X                & \\
\end{tabular}

\noindent
{
  \begin{tabular}{l p{.8\textwidth}}

  \textbf{Field}  & \textbf{Description} \\
  PEN             & Enable pulse output \newline
                    When '1', NP pulses are sent on channel number CHN \newline
                    Bit clears itself after write \\
  CHN             & Channel number to send pulses on \newline
                    Valid values: \textbf{1..6} \\
  NP              & Number of pulses to send on the channel \\
  LDTEN           & Enable pulse LED sequencing \\
  RST             & Reset FPGA logic \newline
                    Writing this bit to 1 will issue a system reset and the communication to the
                    board will be lost for approx. 100~ms \\
  SWITCHES        & Current switch status \newline
                    bit 0 -- SW1.1 \newline
                    bit 1 -- SW1.2 \newline
                    ... \newline
                    bit 7 -- SW2.4 \newline
                    \textbf{1} -- switch is \textbf{OFF} \newline
                    \textbf{0} -- switch is \textbf{ON} \\
  RTM             & RTM detection lines status \newline
                    \textbf{0} -- line active \newline
                    \textbf{1} -- line inactive \\
  CWDTO           & Communication watchdog timeout status \newline
                    \textbf{0} -- watchdog idle \newline
                    \textbf{1} -- communication error has occured and watchdog timer fired \newline
                    This bit is cleared by writing a '1' to it \\
  \textit{Reserved} & Write as '0'; read undefined \\
  \end{tabular}
}

%------------------------------------------------------------------------------
% SUBSEC: PULSE-CNTRS
%------------------------------------------------------------------------------
\subsection{Pulse counters}
\label{app:memmap-pulse-cnt}

Base address: 0x100

\noindent All registers are 32-bit read-only registers.

\begin{table}[h]
    \begin{tabular}{l l p{.6\textwidth}}
    \textbf{Offset} & \textbf{Name} & \textbf{Description} \\
    0x00 & CH1O & Channel 1 output counter \\
    0x04 & CH1I & Channel 1 input counter \\
    0x08 & CH2O & Channel 2 output counter \\
    0x0c & CH2I & Channel 2 input counter \\
    0x10 & CH3O & Channel 3 output counter \\
    0x14 & CH3I & Channel 3 input counter \\
    0x18 & CH4O & Channel 4 output counter \\
    0x1c & CH4I & Channel 4 input counter \\
    0x20 & CH5O & Channel 5 output counter \\
    0x24 & CH5I & Channel 5 input counter \\
    0x28 & CH6O & Channel 6 output counter \\
    0x2c & CH6I & Channel 6 input counter \\
    \end{tabular}
\end{table}


%==============================================================================
% end Appendices
%==============================================================================
\end{appendices}


%==============================================================================
% Bibliography
%==============================================================================
\pagebreak
\bibliographystyle{ieeetr}
\bibliography{hdlg-pts-conv-ttl-rtm-blo}

\end{document}
