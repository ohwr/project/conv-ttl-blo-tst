%==============================================================================
% Document header
%==============================================================================
\documentclass[a4paper,11pt]{article}

% Color package
\usepackage[usenames,dvipsnames,table]{xcolor}

% Appendix package
\usepackage[toc,page]{appendix}

% Hyperrefs
\usepackage[
  colorlinks = true,
  linkcolor  = black,
  citecolor  = black,
  urlcolor   = blue,
]{hyperref}

\usepackage{graphicx}
\usepackage{multirow}

% Header and footer customization
\usepackage{fancyhdr}
\setlength{\headheight}{15.2pt}
\pagestyle{fancy}
\fancyhead[L]{\nouppercase{\leftmark}}
\fancyhead[R]{} 
\renewcommand{\footrulewidth}{0.4pt}

%==============================================================================
% Start of document
%==============================================================================
\begin{document}

%------------------------------------------------------------------------------
% Title
%------------------------------------------------------------------------------
\include{cern-title}

%------------------------------------------------------------------------------
% Licensing info
%------------------------------------------------------------------------------
\pagebreak

\thispagestyle{empty}

\addcontentsline{toc}{section}{Licensing information}
\section*{Licensing information}

\noindent
This document is licensed under a Creative Commons Attribution-ShareAlike 4.0
International License. If you have not received a copy of the license along with this
work, see \\
\url{http://creativecommons.org/licenses/by-sa/4.0/}

%------------------------------------------------------------------------------
% Revision history
%------------------------------------------------------------------------------
\section*{Revision history}
\addcontentsline{toc}{section}{Revision history}


\centerline
{
  \rowcolors{2}{white}{gray!25}
  \begin{tabular}{l c p{.6\textwidth}}
  \hline
  \multicolumn{1}{c}{\textbf{Date}} & \multicolumn{1}{c}{\textbf{Version}} & \multicolumn{1}{c}{\textbf{Change}} \\
  \hline
  24-01-2014 & 0.1 & First draft \\
  13-11-2014 & 0.2 & Second draft, changing link colors, adding licensing information and
                     updating links in the bibliography \\
  16-01-2015 & 1.0 & Added Section~\ref{sec:loopback-mount}, which details the mounting of
                     the card; this section replaces the large text that was there in the document's previous
                     version and that served the same purpose as these pictures \\
  \hline
  \end{tabular}
}

%------------------------------------------------------------------------------
% Table of contents, list of figs, tables
%------------------------------------------------------------------------------
\pagebreak
\pdfbookmark[1]{\contentsname}{toc}
\tableofcontents

\listoffigures
\listoftables

%------------------------------------------------------------------------------
% List of abbreviations
%------------------------------------------------------------------------------
\pagebreak
\section*{List of abbreviations}
\begin{tabular}{l l}
HDL & Hardware Description Language \\
I$^2$C & Inter-Integrated Circuit (bus) \\
OHWR & Open-Hardware Repository \\
PCB & Printed-Circuit Board \\
PTS & Production Test Suite \\
RTM & Rear-Transition Module \\
VME & VERSAmodule Eurocard \\
\end{tabular}
\addcontentsline{toc}{section}{List of abbreviations}

%==============================================================================
% SEC: Intro
%==============================================================================
\pagebreak
\section{Introduction}
\label{sec:intro}

Production Test Suite (PTS) is the environment designed for the functionality tests of
boards at the manufacturing site, right after production. It assures that boards comply
with a minimum set of quality rules in terms of soldering, mounting and PCB fabrication
process.

This document contains information about the hardware of the CONV-TTL-RTM-BLO PTS.

%------------------------------------------------------------------------------
% SUBSEC: Additional docs
%------------------------------------------------------------------------------
\subsection{Additional documentation}

\begin{itemize}
  \item CONV-TTL-BLO User Guide \cite{ctb-ug}
  \item CONV-TTL-BLO Hardware Guide \cite{ctb-hwguide}
  \item CONV-TTL-RTM-BLO PTS User Guide \cite{pts-userguide}
  \item CONV-TTL-RTM-BLO PTS HDL Guide \cite{pts-hdlguide}
\end{itemize}

%==============================================================================
% SEC: PTS system
%==============================================================================
\section{PTS system}
\label{sec:pts-sys}

Since most of the hardware to be tested on the blocking pulse repetition is on the
CONV-TTL-BLO side, only the connectors and LEDs need to be electrically tested on the
RTM board. The PTS system (Figure~\ref{fig:pts-sys}) used to test these components consists
of the test computer and the ELMA crate.

\begin{figure}[h]
  \centerline{\includegraphics[width=.9\textwidth]{fig/pts-sys}}
  \caption{PTS System}
  \label{fig:pts-sys}
\end{figure}

The ELMA crate within the PTS contains a system monitor (SysMon) board that monitors
voltage levels, temperatures and controls fan speeds for the crate fans. The crate
can be accessed via Telnet to send commands to the VME boards inside the crate.
The ELMA SysMon translates these commands into I$^2$C accesses to registers on the
VME board. It is in this manner that the CONV-TTL-BLO can be controlled remotely
in an ELMA crate.

Software on the PTS computer connects to the crate via Telnet and communicates to
the CONV-TTL-BLO, which contains logic that drives the pulse and LED lines on the
CONV-TTL-RTM-BLO. Therefore, most of the hardware consist of the CONV-TTL-BLO and
the RTM under test.

The only piece of hardware that remains is a board that loops output pulses back
to inputs, so the HDL of the PTS can verify whether the RTM passes the test. For
more information on the HDL, see~\cite{pts-hdlguide}.

The RTM Board Tester, which loops outputs back to inputs, is described in the next
section.

%==============================================================================
% SEC: RTM board tester
%==============================================================================
\section{RTM board tester}
\label{sec:loopback}

The RTM board tester is a very simple board that loops back the three outputs on a
channel to three inputs around it. One RTM board tester is needed per each three
channels. Table~\ref{tbl:rtm-sigs} shows how the RTM board tester loops output
signals to inputs.

\begin{table}[h]
  \caption{Loopback connections on RTM board tester}
  \label{tbl:rtm-sigs}
  \centerline
  {
    \begin{tabular}{c c}
    \hline
    \textbf{Input} & \textbf{Outputs} \\
    \hline
    I1 & O11 O32 O23 \\
    I2 & O21 O12 O33 \\
    I3 & O31 O22 O13 \\
    \hline
    \end{tabular}  
  }
\end{table}

Table~\ref{tbl:use-six-chan} shows how two RTM board testers can be used on a
six-channel pulse repeater RTM.

\begin{table}[h]
  \label{tbl:use-six-chan}
  \caption{Output-to-input connections on a six-channel RTM}
  \centerline
  {
    \begin{tabular}{c c c c c}
    \hline
    \textbf{Board} & \textbf{Output} & \multicolumn{3}{c}{\textbf{Input}} \\
    \hline
    \multirow{3}{*}{1} & O1 & I1 & I2 & I3 \\
                       & O2 & I2 & I3 & I1 \\
                       & O3 & I3 & I1 & I2 \\
    \hline
    \multirow{3}{*}{2} & O4 & I4 & I5 & I6 \\
                       & O5 & I5 & I6 & I4 \\
                       & O6 & I6 & I5 & I4 \\
    \hline
    \end{tabular}
  }
\end{table}

%------------------------------------------------------------------------------
\pagebreak
\subsection{Mounting the RTM board tester}
\label{sec:loopback-mount}
%------------------------------------------------------------------------------

The RTM board tester is composed of two boards:

\begin{itemize}
  \item \textbf{the connection board} -- provides the connections in Table~\ref{tbl:rtm-sigs}
  \item \textbf{the support board} -- provides a mechanical support for the Lemo~00 patch-panel
  connectors, as well as acting as the return path for currents passing through these
  connectors
\end{itemize}

The following items are needed to mount one RTM board tester:

\begin{itemize}
  \item 1x connection board
  \item 1x support board
  \item 3x M2.5 screws
  \item 3x M2.5 hexagonal nuts
  \item 12x Lemo FAA.00.250.NTA connectors
\end{itemize}

Follow this procedure in order to mount the card:

\begin{enumerate}
  \item Mount the FAA.00.250.NTA connectors to the support board using their hexagonal nuts:
  \begin{figure}[h]
    \centerline{\includegraphics[width=.7\textwidth]{fig/lemo-to-support.jpg}}
  \end{figure}
  \item Plug the support board on the connection board with the connectors covering the marked
  positions on the connection board:
  \begin{figure}[h]
    \centerline{\includegraphics[width=.7\textwidth]{fig/support-to-connection.jpg}}
  \end{figure}
  \pagebreak
  \item Fix the two boards together using the three screws and hexagonal nuts:
  \begin{figure}[h]
    \centerline{\includegraphics[width=.5\textwidth]{fig/fix-together.jpg}}
  \end{figure}  
  \item Finally, solder the Lemo connectors on their pads on the connection board:
  \begin{figure}[h]
    \centerline{\includegraphics[width=.7\textwidth]{fig/solder.jpg}}
  \end{figure}   
\end{enumerate}

%==============================================================================
% Bibliography
%==============================================================================
\pagebreak
\bibliographystyle{ieeetr}
\bibliography{hwg-pts-conv-ttl-rtm-blo}
\addcontentsline{toc}{section}{References}

\end{document}
