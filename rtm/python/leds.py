##_______________________________________________________________________________________________
##
##                                     CONV-TTL-BLO-RTM PTS
##
##                                         CERN,BE/CO-HT
##_______________________________________________________________________________________________
##
##-----------------------------------------------------------------------------------------------
##
##                                  CONV-TTL-BLO-RTM LEDs test
##
##-----------------------------------------------------------------------------------------------
##
## Description  Testing the rear panel LEDs on the CONV-TTL-BLO-RTM.
##
##              This test script writes the LDTEN bit in the CSR of the PTS firmware on the
##              CONV-TTL-BLO front module driving the RTM. Setting this bit initiates the
##              sequencing of the pulse LEDs on the rear panel.
##
##              The firmware sequences each LED starting from channel 1 and ending with channel 6
##              and the user is asked to confirm all LEDs light up, for the test to pass.
##
## Authors      Theodor-Adrian Stana (t.stana@cern.ch)
##
## Website      http://www.ohwr.org/projects/pts
## Date         12/04/2013
##-----------------------------------------------------------------------------------------------
##
##------------------------------------------------------------------------------------------------
##                               GNU LESSER GENERAL PUBLIC LICENSE
##                              ------------------------------------
## This source file is free software; you can redistribute it and/or modify it under the terms of
## the GNU Lesser General Public License as published by the Free Software Foundation; either
## version 2.1 of the License, or (at your option) any later version.
## This source is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
## without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
## See the GNU Lesser General Public License for more details.
## You should have received a copy of the GNU Lesser General Public License along with this
## source; if not, download it from http://www.gnu.org/licenses/lgpl-2.1.html
##-------------------------------------------------------------------------------------------------

##-------------------------------------------------------------------------------------------------
##                                            Import
##-------------------------------------------------------------------------------------------------

# Import system modules
import sys
import time
import os, errno, re, sys, struct
import os.path
import traceback

# Import common modules
from ctypes import *
from ptsexcept import *
from vv_pts import *
from pts_memmap import *

##-------------------------------------------------------------------------------------------------
##                                             main                                              --
##-------------------------------------------------------------------------------------------------

def main(bus,tname,inf,log):

    """
    tests : Rear panel LEDs
    uses  : pts_conv_ttl_blo_rtm.bit and leds.py
    """

    pel = PTS_ERROR_LOGGER(inf,log)

    try:

        # Enable pulse LED sequencing
        bus.vv_write(CSR, 1 << CSR_LDTEN_OFS)

        # The firmware should blink the LEDs, ask the operator for input
        inp = raw_input("--> Are the channel LEDs blinking one by one? yes/no: ")
        while True:
            if inp.find("yes") != -1 or inp.find("YES") != -1:
                break

            if inp.find("no") != -1 or inp.find("NO") != -1:
               msg = "ERROR: Rear panel LEDs"
               pel.set(msg)
               break

            inp = raw_input("Please type 'yes' or 'no' to continue:")

        return pel.get()

    except BusException, e:
        raise PtsError("SKT Exception: %s" % (e))

    except BusWarning, e:
        raise PtsError("SKT Warning: %s" % (e))
