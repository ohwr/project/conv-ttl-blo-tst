#===============================================================================
# CERN (BE-CO-HT)
# PTS memory map
#===============================================================================
# author: Theodor Stana (t.stana@cern.ch)
#
# date of creation: 2014-01-14
#
# version: 1.0
#
# description:
#     This module contains register address definitions that are used across the
#     various tests. Importing this module inside a test script makes these
#     definitions available for use within a bus.vv_write or bus.vv_read method
#     (see vv_pts.py for these methods).
#
# dependencies:
#     none.
#
# references:
#     none.
#
#===============================================================================
# GNU LESSER GENERAL PUBLIC LICENSE
#===============================================================================
# This source file is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation; either version 2.1 of the License, or (at your
# option) any later version. This source is distributed in the hope that it
# will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Lesser General Public License for more details. You should have
# received a copy of the GNU Lesser General Public License along with this
# source; if not, download it from http://www.gnu.org/licenses/lgpl-2.1.html
#===============================================================================
# last changes:
#    2014-01-14    Theodor Stana     t.stana@cern.ch     File created
#===============================================================================
#  TODO: -
#===============================================================================

# Registers used across all test programs
BOARD_ID_REG  = 0x000
CSR           = 0x004
CSR_PEN_OFS   = 0
CSR_CHN_OFS   = 1
CSR_NP_OFS    = 4
CSR_LDTEN_OFS = 7
CSR_RST_OFS   = 15

# Pulse counter base address, used in blo_pulse.py
PULSE_CNT_BASE = 0x100

