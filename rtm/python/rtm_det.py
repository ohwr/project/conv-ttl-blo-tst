##_______________________________________________________________________________________________
##
##                                     CONV-TTL-BLO-RTM PTS
##
##                                         CERN,BE/CO-HT
##_______________________________________________________________________________________________
##
##-----------------------------------------------------------------------------------------------
##
##                                CONV-TTL-BLO RTM detection lines test
##
##-----------------------------------------------------------------------------------------------
##
## Description  This test script checks the RTM detection lines by reading the RTM field in the
##              CSR of the PTS firmware downloaded to the CONV-TTL-BLO. It then checks that the
##              RTM detection lines match a known board combination, as per the RTM detection
##              lines webpage on OHWR:
##              www.ohwr.org/projects/conv-ttl-blo/wiki/rtm_board_detection
##
##              If they match a combination, this combination is listed in the PTS .inf file for
##              the board. Otherwise, an error is flagged and the state of the RTM lines is
##              printed in the same file.
##
## Authors      Theodor-Adrian Stana (t.stana@cern.ch)
## Website      http://www.ohwr.org/projects/pts
## Date         24/01/2014
##-----------------------------------------------------------------------------------------------
##
##------------------------------------------------------------------------------------------------
##                               GNU LESSER GENERAL PUBLIC LICENSE
##                              ------------------------------------
## This source file is free software; you can redistribute it and/or modify it under the terms of
## the GNU Lesser General Public License as published by the Free Software Foundation; either
## version 2.1 of the License, or (at your option) any later version.
## This source is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
## without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
## See the GNU Lesser General Public License for more details.
## You should have received a copy of the GNU Lesser General Public License along with this
## source; if not, download it from http://www.gnu.org/licenses/lgpl-2.1.html
##-------------------------------------------------------------------------------------------------

##-------------------------------------------------------------------------------------------------
##                                            Import
##-------------------------------------------------------------------------------------------------

# Import system modules
import sys
import time
import os, errno, re, sys, struct
import os.path
import traceback

# Import common modules
from ctypes import *
from ptsexcept import *
from vv_pts import *
from pts_memmap import *


##-------------------------------------------------------------------------------------------------
##                                             main                                              --
##-------------------------------------------------------------------------------------------------
def main(bus, tname, inf, log):

    """
    tests : RTM detection lines
    uses  : pts_conv_ttl_blo_rtm.bit and rtm_det.py
    """

    pel = PTS_ERROR_LOGGER(inf, log)

    try:
        rtmdet = bus.vv_read(CSR) & 0x3f000000
        rtmdet >>= 24

        rtmp = rtmdet & 0x38
        rtmm = rtmdet & 0x07

        if (rtmm == 0x01) and (rtmp == 0x00):
            msg = "Detected CONV-TTL-RTM with blocking piggyback.\n"
            inf.write(msg)
        elif (rtmm == 0x01) and (rtmp == 0x01):
            msg = "Detected CONV-TTL-RTM with RS-485 piggyback.\n"
            inf.write(msg)
        elif (not rtmm) and (not rtmp):
            msg = "ERROR: RTM detection lines read incorrect or unknown value: 0x%02x." % rtmdet
            pel.set(msg)

        # Finally, return the number of errors that occured
        return pel.get()

    except BusException, e:
        raise PtsError("SKT Exception: %s" % (e))

    except BusWarning, e:
        raise PtsError("SKT Warning: %s" % (e))

