##_______________________________________________________________________________________________
##
##                                      CONV-TTL-BLO-RTM PTS
##
##                                         CERN,BE/CO-HT
##_______________________________________________________________________________________________
##
##-----------------------------------------------------------------------------------------------
##
##                                CONV-TTL-BLO-RTM pulse test
##
##-----------------------------------------------------------------------------------------------
##
## Description  This module implements the blocking pulse test of the CONV-TTL-BLO-RTM PTS.
##
##              The pulse test uses two RTM board testers which loops back each output to three
##              adjacent outputs, as seen below:
##
##                  O1 I1 I2 I3
##                  O2 I2 I3 I1
##                  O3 I3 I1 I2
##                  O4 I4 I5 I6
##                  O5 I5 I6 I4
##                  O6 I6 I4 I5
##
##              The firmware inside the driving CONV-TTL-BLO waits for this script to write the
##              number of pulses to send on a channel of choice and enable pulse sending. Then,
##              through the RTM board tester, a pulse signal is looped back to the inputs as seen
##              above. Each time a pulse is output an output pulse counter is incremented, and
##              each time a pulse is received on a channel an input pulse counter is incremented.
##
##              For the memory map of the pulse counters, see the memory map in the HDL guide.
##
##              This script cycles through each of the channels in turn and initiates the sending
##              of seven pulses. It then waits one second and starts reading the input pulse
##              counters. On each channel, a theoretical number of pulses sent is calculated and
##              then the input counter is verified versus this theoretical number of pulses. If
##              the input counter matches it, the test passes, otherwise it fails.
##
##              Seeing as how on each channel we send seven pulses, a cycle for the first two
##              channels would look like this:
##                - send 7 pulses on channel 1
##                - output counter on channel 1 should show 7
##                - input counters on channels 1, 2 and 3 should show 7
##                - send 7 pulses on channel 2
##                - output counter on channel 2 should show 7
##                - input counters on channels 1, 2 and 3 should show 14
##
##              Both the input and output pulse counters values are stored to the output .inf file
##              and an error is flagged appropriately, should it occur.
##
## Authors      Theodor-Adrian Stana (t.stana@cern.ch)
## Website      http://www.ohwr.org/projects/pts
## Date         24/01/2014
##-----------------------------------------------------------------------------------------------
##
##------------------------------------------------------------------------------------------------
##                               GNU LESSER GENERAL PUBLIC LICENSE
##                              ------------------------------------
## This source file is free software; you can redistribute it and/or modify it under the terms of
## the GNU Lesser General Public License as published by the Free Software Foundation; either
## version 2.1 of the License, or (at your option) any later version.
## This source is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
## without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
## See the GNU Lesser General Public License for more details.
## You should have received a copy of the GNU Lesser General Public License along with this
## source; if not, download it from http://www.gnu.org/licenses/lgpl-2.1.html
##-------------------------------------------------------------------------------------------------

##-------------------------------------------------------------------------------------------------
##                                            Import
##-------------------------------------------------------------------------------------------------

# Import system modules
import sys
import time
import os, errno, re, sys, struct
import os.path
import traceback

# Import common modules
from ctypes import *
from ptsexcept import *
from vv_pts import *
from pts_memmap import *

class CPulseCounter:

    def __init__(self, bus, base):
        self.bus  = bus
        self.base = base

    def wr_reg(self, addr, val):
        self.bus.vv_write(self.base + addr,val)

    def rd_reg(self, addr):
        return self.bus.vv_read(self.base + addr)

    def rd_out_cnt(self, chan):
        return self.rd_reg((chan-1)*8)

    def rd_in_cnt(self, chan):
        return self.rd_reg((chan-1)*8 + 4)

##-------------------------------------------------------------------------------------------------
##                                             main                                              --
##-------------------------------------------------------------------------------------------------
def main(bus, tname, inf, log):

    """
    tests : Blocking pulse repetition and output connectors
    uses  : pts_conv_ttl_blo_rtm.bit and blo_pulse.py
    """

    pel = PTS_ERROR_LOGGER(inf, log)

    inf.write("Pulse counter values:\n")
    inf.write("---------------------\n")

    try:
        # Initialize a pulse counter object
        pc = CPulseCounter(bus, PULSE_CNT_BASE)

        # Number of pulses to send
        nr_pulses = 7

        # Init theoretical input count variable and counter for zeroes in ic_arr
        ic_sim   = 0
        zero_cnt = 0

        for i in range(1, 7):
            # First, set the start index based on the channel where the RTM board
            # tester is placed:
            #   CH1->3: board tester 1
            #   CH4->6: board tester 2
            si = 1
            if i >= 4:
                si = 4

            # End index is always start index + 3, due to RTM board tester looping
            # back three channels at a time
            ei = si+3

            # Clear theoretical input counter array on the fourth channel, since
            # this is where the second board tester is
            if (i == 4):
                ic_sim   = 0
                zero_cnt = 0

            # Compute theoretical number of pulses for the channel based on the number of
            # pulses sent
            ic_sim += nr_pulses

            # Send pulses on the current output channel
            val = (1 << CSR_PEN_OFS) | (i << CSR_CHN_OFS) | (nr_pulses << CSR_NP_OFS)
            bus.vv_write(CSR, val)

            # wait one second, then start reading input and output counter values
            time.sleep(1)

            # Read the channel registers, blocking channels 1-6 correspond to pulse
            # counter channels 1-6
            ic_arr = []
            oc_arr = []
            for j in range(si, ei):
                ic_arr.append(pc.rd_in_cnt(j))
                oc_arr.append(pc.rd_out_cnt(j))

            # Check for all zeroes in input count
            if all(j == 0 for j in ic_arr):
                zero_cnt += 1

            # Compare input count array with theoretical input count
            for j in range(3):
                if (ic_arr[j] == ic_sim):
                    msg = "good : O%d = %2d / I%d = %2d\n" % (i, oc_arr[i-si], (si+j)%(ei), ic_arr[j])
                    inf.write(msg)
                else:
                    msg = "ERROR: O%d = %2d / I%d = %2d - expected %2d" % (i, oc_arr[i-si], (si+j)%(ei), ic_arr[j], ic_sim)
                    pel.set(msg)

            # Check for all zeroes on all three channels in a group
            if zero_cnt == 3:
                msg = "ERROR: No pulses received on O%d, O%d, O%d. Check RTM board tester and/or front module board blocking power supply." % (i-2, i-1, i)
                pel.set(msg)

        # Finally, return the number of errors that occured
        return pel.get()

    except BusException, e:
        raise PtsError("SKT Exception: %s" % (e))

    except BusWarning, e:
        raise PtsError("SKT Warning: %s" % (e))

